import React from 'react';
import {
    Image,
    
    Menu,  
   } from 'semantic-ui-react'

export default props => (
    <div className="grid">   
    
    <Menu fixed="top" className="top-menu">
        <Menu.Item className="logo-space-menu-item">
          <div className="display-inline logo-space">
            <Image src="./neo-logo.png" />
            {/* <p>Ferexim</p> */}
          </div>
        </Menu.Item>

       
      </Menu>
       {props.children}   
    </div>
);
