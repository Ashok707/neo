import React, {Component} from 'react'
import {Menu} from 'semantic-ui-react'
import {Link} from "react-router-dom";
import TextIcon from "../extension/TextIcon";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import { actionCreators } from "../../Model/SideMenu";

class SideMenu extends Component {
    state = {
        activeItem: 'neoreports',
    };

    handleItemClick = (e, {name}) => this.setState({activeItem: name});
    changeSize = () => this.setState({smallSidebar: !this.props.smallMenu});

    getMenu() {
        const {activeItem} = this.state;
        return (
            <Menu fixed='left' borderless className={(this.props.smallMenu ? 'small-side' : '') + ' side'} vertical>
                <Menu.Item as={Link} to={'/neoreports'} name='neoreports' 
                 active={activeItem === 'neoreports'}
                           onClick={this.handleItemClick}>
                    <TextIcon hideText={this.props.smallMenu}  name='chart bar'>
                        Neo Reports
                    </TextIcon>
                </Menu.Item>

                <Menu.Item as={Link} to={'/logicalview'} name='logicalview'
                           active={activeItem === 'logicalview'}
                           onClick={this.handleItemClick}>
                    <TextIcon hideText={this.props.smallMenu} name='list'>
                        Views
                    </TextIcon>
                </Menu.Item>

                
              
                <Menu.Item as={Link} to={'/data_source'} name='data_source' active={activeItem === 'data_source'}
                           onClick={this.handleItemClick}>

                    <TextIcon hideText={this.props.smallMenu} name='database'>
                        Data Source
                    </TextIcon>
                </Menu.Item>

                <Menu.Item as={Link} to={'/security'} name='security' active={activeItem === 'security'}
                           onClick={this.handleItemClick}>
                    <TextIcon hideText={this.props.smallMenu} name='user'>
                        Security
                    </TextIcon>

                </Menu.Item>
            </Menu>
        )
    }

    render() {
        return (
            <div className='parent'>
                <div className={(this.props.smallMenu ? 'small-side ' : '') + 'side'}>
                    {this.getMenu()}
                </div>
                <div className={(this.props.smallMenu ? 'small-content ' : '') + 'content'}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default connect(
    state => state.sideMenu,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(SideMenu);
