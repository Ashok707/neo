import React, { Component } from "react";
import { Icon, Menu, Rail } from "semantic-ui-react";
import { NavLink } from "react-router-dom";

export default class VerticalMenu extends Component {
  state = { activeItem: "dashboard" };
  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const { activeItem } = this.state;

    return (
      <Menu fluid inverted vertical>
        <NavLink to={"/logicalview"}>
          <Menu.Item
            name="dashboard"
            active={activeItem === "dashboard"}
            onClick={this.handleItemClick}
          >
            <Rail position="left">
              <Icon color="teal" name="home" />
            </Rail>
            Dashboard
          </Menu.Item>
        </NavLink>

        <NavLink to={"/logicalview"} exact>
          <Menu.Item
            name="logicalview"
            active={activeItem === "logicalview"}
            onClick={this.handleItemClick}
          >
            <Icon name="logicalview" />
            Logicalview
          </Menu.Item>
        </NavLink>

        <NavLink to={"/patients"} exact>
          <Menu.Item
            name="patients"
            active={activeItem === "patients"}
            onClick={this.handleItemClick}
          >
            <Icon name="users" />
            Patients
          </Menu.Item>
        </NavLink>

        <NavLink to={"/Datasource"} exact>
          <Menu.Item
            name="Datasource"
            active={activeItem === "Datasource"}
            onClick={this.handleItemClick}
          >
            Datasource
          </Menu.Item>
        </NavLink>

        <NavLink to={"/layout"} exact>
          <Menu.Item
            name="layout"
            active={activeItem === "layout"}
            onClick={this.handleItemClick}
          >
            <Icon name="home" />
            Layout
          </Menu.Item>
        </NavLink>
        <NavLink to={"/login"} exact>
          <Menu.Item
            name="login"
            active={activeItem === "login"}
            onClick={this.handleItemClick}
          >
            <Icon name="home" />
            login
          </Menu.Item>
        </NavLink>
      </Menu>
    );
  }
}
