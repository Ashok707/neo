﻿import React from 'react'
import { Route, Switch } from 'react-router-dom';

      //****Home Layout****/
import Defaultlayout from '../layout/DefaultLayout'
      //Pages

import LoginPage from '../Pages/login'
import createdb from '../Pages/Create_Db'


      //****Home Layout****/
import HomeLayout from './HomeLayout'
      //Pages
import Reports from '../Pages/neo_reports'
import LogicalView from '../Pages/LogicalView'
import LogicalView1 from '../Pages/LogicalView1'
import LogicalView2 from '../Pages/LogicalView2'
import PhysicalView from '../Pages/PhysicalView'

import Security from '../Pages/Security'

      //****User Layout****/
import UserLayout from '../layout/UserLayout';
      //Pages
import filter from '../Pages/filter';
import manage from '../Pages/manage';
import Home from '../Pages/Home'
import node from '../Pages/node'
import datasource1 from '../Pages/data_source'





function RouteWithLayout({layout, component, ...rest}){
  return (
    <Route {...rest} render={(props) =>
      React.createElement( layout, props, React.createElement(component, props))
    }/>
  );
}

export default () => (
  <div>   
    <Switch>    
    
    <RouteWithLayout layout={Defaultlayout} path="/login" component={LoginPage}/>

    <RouteWithLayout layout={HomeLayout} path="/neoreports" component={Reports}/>
    <RouteWithLayout layout={HomeLayout} path="/LogicalView" component={LogicalView}/>
    <RouteWithLayout layout={HomeLayout} path="/LogicalView1" component={LogicalView1}/>
    <RouteWithLayout layout={HomeLayout} path="/LogicalView2" component={LogicalView2}/>
    <RouteWithLayout layout={HomeLayout} path="/PhysicalView" component={PhysicalView}/>    
    
    
    <RouteWithLayout layout={HomeLayout} path="/security" component={Security}/>
    <RouteWithLayout layout={HomeLayout} path="/data_source" component={datasource1}/>

    <RouteWithLayout layout={UserLayout} path="/home" component={Home}/>
    <RouteWithLayout layout={UserLayout} path="/filter" component={filter}/>
    <RouteWithLayout layout={UserLayout} path="/manage" component={manage}/>
    <RouteWithLayout layout={UserLayout} path="/node" component={node}/>
    <RouteWithLayout layout={UserLayout} path="/datasource" component={createdb}/>
     <RouteWithLayout layout={Defaultlayout} path="/" component={LoginPage}/>
    
</Switch>

    

  </div>
);
