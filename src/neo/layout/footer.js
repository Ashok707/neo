import React, {Component} from 'react';
import {Link} from 'react-router-dom'
//import  {Footer}  from 'react-materialize';
//import '../../resource/template.css'
import {Segment,Container,Grid,Header,List,Divider,Image} from 'semantic-ui-react';

class foot extends Component{
  render(){
return (
<div>
<Segment inverted  >
      <Container textAlign='center'>
        <List horizontal inverted divided link size='small'>
          <List.Item as='a' href='#'>
            Site Map
          </List.Item>
          <List.Item as='a' href='#'>
            Contact Us
          </List.Item>
          <List.Item as='a' href='#'>
            Terms and Conditions
          </List.Item>
          <List.Item as='a' href='#'>
            Privacy Policy
          </List.Item>
        </List>
      </Container>
    </Segment>
</div>
);
 }
}
export default foot;
