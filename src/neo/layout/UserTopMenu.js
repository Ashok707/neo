import React, {Component} from 'react'
import {Icon,
        Image,
      
        Menu,  
       } from 'semantic-ui-react'
import {actionCreators as sideAction} from "../../Model/SideMenu";
import {actionCreators as searchAction} from "../../Model/SearchStore";
import {bindActionCreators} from "redux";
import { connect } from 'react-redux';

import MediaQuery from 'react-responsive';
class TopMenu extends Component {
  state = {};

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  doSearch(event) {
      this.props.actions.search(event.target.value);  
  }

  render() {
    return (
      <div>
      <Menu fixed="top" className="top-menu">
        <Menu.Item className="logo-space-menu-item">
          <div className="display-inline logo-space">
            <Image src="./neo-logo.png" />
            {/* <p>Ferexim</p> */}
          </div>
        </Menu.Item>

        <Menu.Item
          className="no-border"
          onClick={this.props.actions.toggleSideMenu}
        >
          <Icon name="bars" />
        </Menu.Item>

        <Menu.Menu className="center-top-menu" >
        <Menu.Item className="no-border" ><a href="/home">Home</a></Menu.Item>
        <Menu.Item className="no-border" >|</Menu.Item>
        <Menu.Item className="no-border" >
        <a href="/home">Support</a>
        </Menu.Item>    
        <Menu.Item className="no-border" >|</Menu.Item>
        <Menu.Item className="no-border" >
        <a href="/home">Blog</a>
        </Menu.Item>
       
       
        </Menu.Menu>
        <Menu.Menu position="right">
    
          <Menu.Item className="no-border" position="right">
            <div className="display-inline">
              <Image
                circular
                size={"mini"}
                src="./default-user.png"
              />
              User
            </div>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
      <MediaQuery minDeviceWidth={1224}>
      
      <Menu fixed="top" className="top-menu">
        <Menu.Item className="logo-space-menu-item">
          <div className="display-inline logo-space">
            <Image src="./neo-logo.png" />           
          </div>
        </Menu.Item>

        {/* <Menu.Item
          className="no-border"
          onClick={this.props.actions.toggleSideMenu}
        >
          <Icon name="bars" />
        </Menu.Item> */}

        <Menu.Menu className="center-top-menu" >
        <Menu.Item className="no-border" ><a href="/home">Home</a></Menu.Item>
        <Menu.Item className="no-border" >|</Menu.Item>
        <Menu.Item className="no-border" >
        <a href="/home">Support</a>
        </Menu.Item>    
        <Menu.Item className="no-border" >|</Menu.Item>
        <Menu.Item className="no-border" >
        <a href="/home">Blog</a>
        </Menu.Item>
       
       
        </Menu.Menu>
        <Menu.Menu position="right">
    
          <Menu.Item className="no-border" position="right">
            <div className="display-inline">
              <Image
                circular
                size={"mini"}
                src="./default-user.png"
              />
              User
            </div>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
      </MediaQuery>
      </div>
    );
  }
}

export default connect(
    state => state.sideMenu,
     dispatch => {
        return {
            actions: bindActionCreators(Object.assign({}, sideAction, searchAction), dispatch)
        }}
)(TopMenu);
