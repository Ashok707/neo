import React from 'react'
import { Route } from 'react-router'
import Layout from '.././layout/Layout'
import Home from '.././pages/Home'
import PhysicalView from '../pages/PhysicalView'
import Datasource from '.././pages/Datasource'
import FetchData from '.././FetchData'
import HomepageLayout from '.././pages/HomepageLayout'
import LoginPage from '.././pages/login'
import NodePage from '../Grid/node'
//import MainPage from '../main'

export default () => (  

  <Layout>
    <Route exact path="/" component={Home} />
    {/* <Route path="/main" component={MainPage} /> */}
    <Route path="/PhysicalView" component={PhysicalView} />
    <Route path="/Datasource" component={Datasource} />
    <Route path="/layout" component={HomepageLayout} />
    <Route path="/fetchdata/:startDateIndex?" component={FetchData} />
   
    <Route path="/node" component={NodePage}/>

  </Layout>
);
