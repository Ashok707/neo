import React from 'react'
import { Segment, Input } from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css';
import $ from 'jquery';
import kendo from '@progress/kendo-ui';
import '../../styles/logicalcard.css';
import { getlogicalData, getlogicalDatadigram } from '../../services/reportService';
import { TabStrip, TabStripTab } from '@progress/kendo-react-layout';


class Logicalview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      connections: [],
      childVisible: false,
      data: []
      , masterdata: []
      , filterdata: []
      , ChildGridData: []
      , physicalview: []
      , selected: 0
      , visible: true
      , collapse: true
    };
    this.handleKeywordsChange = this.handleKeywordsChange.bind(this);
  }

  componentDidMount() {
    $("#loading").prepend("<div class='k-loading-mask' style='width:100%;height:100%'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'><div class='k-loading-color'></div></div></div>");
    getlogicalData()
      .then((response) => {
        this.setState({ filterdata: response.data, masterdata: response.data });
        $("#loading").find("div.k-loading-mask").remove();
      })
  }

  componentWillMount() {
  }

  handleSelect = (e) => {
    this.setState({ selected: e.selected })
    let diagramName = '';
    if (e.selected === 0)
      diagramName = "logicaldiagram";
    else if (e.selected === 1)
      diagramName = "physicaldiagram";

    const user = {
      path: window.localStorage.getItem('ser_path'),
      //caption: window.localStorage.getItem('ser_caption'),
      server_id: window.localStorage.getItem('ser_id')
    }
     var visualTemplate =(options) =>{
      var dataviz = kendo.dataviz;
      var g = new dataviz.diagram.Group();
      var dataItem = options.dataItem;
      var currY = 30

      g.append(new dataviz.diagram.Rectangle({
        width: 150,
        height: 30,
        stroke: {
          width: 1,
          color: "#3399dd"
        },
        fill: {
          color: "#3399dd"
        }
      }));

      g.append(new dataviz.diagram.TextBlock({
        text: dataItem.name.toUpperCase(),
        // text: <i class="fa fa-bars"></i>,
        x: 40,
        y: 10,
        fill: "#fff",
        fontSize: 10,
      }));

      for (var i = 0; i < dataItem.columns.length; i++) {
        currY += 15;
        g.append(new dataviz.diagram.TextBlock({
          text: dataItem.columns[i].name,
          x: 5,
          y: currY,
          fill: dataItem.columns[i].color || "#000",
          fontWeight: dataItem.columns[i].bold || "",
          fontSize: 10
        }));
        g.append(new dataviz.diagram.TextBlock({
          text: dataItem.columns[i].type,
          x: 100,
          y: currY,
          fill: dataItem.columns[i].color || "#000",
          fontWeight: dataItem.columns[i].bold || "",
          fontSize: 10
        }));
      }
      g.append(new dataviz.diagram.Rectangle({
        width: 150,
        height: dataItem.columns.length + currY + 15,
        stroke: {
          width: 1,
          color: "#3399dd"
        },
        fill: {
        }
      }));
      $("#loading").find("div.k-loading-mask").remove();
      return g;
    }
    $("#loading").prepend("<div class='k-loading-mask' style='width:100%;height:100%'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'><div class='k-loading-color'></div></div></div>");
    getlogicalDatadigram(user, diagramName)
      .then((response) => {
        let diagramdata = response.data;
        let diagramconnection = [];
        var input = diagramdata;
        var connectto = '';
        let MasterConnection = [];
        for (var i = 0; i < input.length; i++) {
          if (e.selected === 0) {
            if (input[i].to) {
              connectto = input[i].to.length;
              for (var lj = 0; lj < connectto; lj++) {
                var row = {}
                row["from"] = input[i].id;
                row["to"] = input[i].to[lj];
                row["text"] = "";
               
                diagramconnection.push(row);
                MasterConnection.push(row);
              }
            }
          }
          else if (e.selected === 1) {
            if (input[i].foriegn_key_relationship) {
              connectto = input[i].foriegn_key_relationship.length;
              for (var pj = 0; pj < connectto; pj++) {
                var totableid = input[i].foriegn_key_relationship[pj].destination.tableId;
                for (var pk = 0; pk < input.length; pk++) {
                  if (input[pk].id === totableid) {
                    var prow = {}
                    prow["from"] = input[i].foriegn_key_relationship[pj].source.tableId;
                    prow["to"] = totableid;
                    prow["text"] = input[i].foriegn_key_relationship[pj].destination.name;
                    prow["srcColId"] = input[i].foriegn_key_relationship[pj].source.colId;
                    prow["desColId"] = input[i].foriegn_key_relationship[pj].destination.colId;
                    prow["fname"] = input[i].foriegn_key_relationship[pj].source.tableName;
                    prow["tname"] = input[i].foriegn_key_relationship[pj].destination.tableName;
                    diagramconnection.push(prow);
                  }
                }
              }
            }
          }
        }
        //console.log(diagramconnection,"diagramconnection");
        //Diagram
        let MasterDiagramData = [];
        //let connectorsarrows=[];
        let xaxis = 50, yaxis = 50;
        let order = 1;
        let Layer = [];
        //End       
        const map = new Map();
        const diagramdataMapping = new Map();
        //#region Logical Layer
        if (e.selected === 0) {
          for (var dc = 0; dc < diagramconnection.length; dc++) {
            if (!map.has(diagramconnection[dc].from)) {
              let inputIdx;
              for (let idx = 0; idx < diagramdata.length; idx++) {
                if (diagramdata[idx].id === diagramconnection[dc].from) {
                  inputIdx = idx;
                }
              }
              if (!diagramdataMapping.has(diagramdata[inputIdx].id)) {
                map.set(diagramdata[inputIdx].id, true);
                diagramdataMapping.set(diagramdata[inputIdx].id, true);
                diagramdata[inputIdx].x = xaxis; diagramdata[inputIdx].y = yaxis;
                MasterDiagramData.push(diagramdata[inputIdx]);
                Layer.push({ layer: order, id: diagramdata[inputIdx].id, toid: "", x: xaxis, y: yaxis });
              }
              if (diagramdata[inputIdx].to.length) {
                let tolength = diagramdata[inputIdx].to.length;
                order++; xaxis += 300;
                for (let toIdx = 0; toIdx < tolength; toIdx++) {
                  let inputtoIdx;
                  for (let idx = 0; idx < diagramdata.length; idx++) {
                    if (diagramdata[idx].id === diagramdata[inputIdx].to[toIdx]) {
                      inputtoIdx = idx;
                    }
                  }
                  if (!diagramdataMapping.has(diagramdata[inputtoIdx].id)) {
                    for (let di = 0; di < Layer.length; di++) {
                      if (diagramdata[inputIdx].id === Layer[di].id) {
                        for (let diq = 0; diq < Layer.length; diq++) {
                          if (Layer[di].layer + 1 === Layer[diq].layer) {
                            xaxis = Layer[diq].x;
                            yaxis = Layer[diq].y;
                          }
                        }
                      }
                    }
                    diagramdataMapping.set(diagramdata[inputtoIdx].id, true);
                    diagramdata[inputtoIdx].x = xaxis; diagramdata[inputtoIdx].y = yaxis;
                    MasterDiagramData.push(diagramdata[inputtoIdx]);
                    yaxis += (MasterDiagramData[MasterDiagramData.length - 1].columns.length) * 15 + 75;
                    Layer.push({ layer: order, id: diagramdata[inputIdx].to[toIdx], toid: diagramdata[inputtoIdx].id, x: xaxis, y: yaxis })
                  }
                }
                yaxis = 50;
              }
            }
          }
          //console.log(MasterDiagramData,"MasterDiagramData");
          if (diagramdata.length > MasterDiagramData.length) {
            for (let idd = 0; idd < diagramdata.length; idd++) {
              let ntabid;
              if (MasterDiagramData.length > 0)
                for (let idn = 0; idn < MasterDiagramData.length; idn++) {
                  if (MasterDiagramData[idn].id === diagramdata[idd].id) {
                    ntabid = true;
                    break;
                  }
                  else {
                    ntabid = false;
                  }
                }
              if (!ntabid) {
                xaxis += 300;
                diagramdata[idd].x = xaxis; diagramdata[idd].y = yaxis;
                MasterDiagramData.push(diagramdata[idd]);
              }
             
            }
          }
        }
        //#endregion
        //#region physical Layer
        else if (e.selected === 1) {
          //physical Layer
          //console.log(diagramconnection,"physical-MasterDiagramData");
          let nic = 0;
          for (let dc = 0; dc < diagramconnection.length; dc++) {
            if (!map.has(diagramconnection[dc].from)) {
              let inputIdx;
              for (let idx = 0; idx < diagramdata.length; idx++) {
                if (diagramdata[idx].id === diagramconnection[dc].from) {
                  inputIdx = idx;
                }
              }
              if (!diagramdataMapping.has(diagramdata[inputIdx].id)) {
                map.set(diagramdata[inputIdx].id, true);
                diagramdataMapping.set(diagramdata[inputIdx].id, true);
                diagramdata[inputIdx].x = xaxis; diagramdata[inputIdx].y = yaxis;
                MasterDiagramData.push(diagramdata[inputIdx]);
                Layer.push({ layer: order, id: diagramdata[inputIdx].id, toid: "", x: xaxis, y: yaxis });
              }
              if (diagramdata[inputIdx].foriegn_key_relationship.length) {
                let tolength = diagramdata[inputIdx].foriegn_key_relationship.length;
                order++; xaxis += 300;
                for (var toIdx = 0; toIdx < tolength; toIdx++) {
                  let inputtoIdx;
                  for (var tidx = 0; tidx < diagramdata.length; tidx++) {
                    if (diagramdata[tidx].id === diagramdata[inputIdx].foriegn_key_relationship[toIdx].destination.tableId) {
                      inputtoIdx = tidx;
                    }
                  }
                  if (inputtoIdx || inputtoIdx === 0)
                    if (!diagramdataMapping.has(diagramdata[inputtoIdx].id)) {
                      for (var di = 0; di < Layer.length; di++) {
                        if (diagramdata[inputIdx].id === Layer[di].id) {
                          for (var diq = 0; diq < Layer.length; diq++) {
                            if (Layer[di].layer + 1 === Layer[diq].layer) {
                              xaxis = Layer[diq].x;
                              yaxis = Layer[diq].y;
                            }
                          }
                        }
                      }
                      diagramdataMapping.set(diagramdata[inputtoIdx].id, true);
                      diagramdata[inputtoIdx].x = xaxis; diagramdata[inputtoIdx].y = yaxis;
                      MasterDiagramData.push(diagramdata[inputtoIdx]);
                      yaxis += (MasterDiagramData[MasterDiagramData.length - 1].columns.length) * 15 + 75;
                      Layer.push({ layer: order, id: diagramdata[inputIdx].foriegn_key_relationship[toIdx].destination.tableId, toid: diagramdata[inputtoIdx].id, x: xaxis, y: yaxis })
                    }
                }
                yaxis = 50;
              }
            }
          }
          //console.log(MasterDiagramData,"physical-MasterDiagramData");
          for (var conId = 0; conId < diagramconnection.length; conId++) {
            var Connectionrow = {}
            let inputIdx, inputtoIdx, srcColIdx, desColIdx;
            let FromX, FromY, ToX, ToY;
            for (var sidx = 0; sidx < MasterDiagramData.length; sidx++) {
              if (MasterDiagramData[sidx].id === diagramconnection[conId].from) {
                
                inputIdx = sidx;
              }
            }
            for (var col = 0; col < MasterDiagramData[inputIdx].columns.length; col++) {
              if (MasterDiagramData[inputIdx].columns[col].id === diagramconnection[conId].srcColId) {
                srcColIdx = col + 1;
                FromX = MasterDiagramData[inputIdx].x + 150;
                FromY = MasterDiagramData[inputIdx].y + 55 + (srcColIdx * 10);
                break;
              }
            }
            for (var didx = 0; didx < MasterDiagramData.length; didx++) {
              if (MasterDiagramData[didx].id === diagramconnection[conId].to) {
                
                inputtoIdx = didx;
              }
            }
            for (var dcol = 0; dcol < MasterDiagramData[inputtoIdx].columns.length; dcol++) {
              if (MasterDiagramData[inputtoIdx].columns[dcol].id === diagramconnection[conId].desColId) {
                desColIdx = dcol + 1;
                ToX = MasterDiagramData[inputtoIdx].x;
                ToY = MasterDiagramData[inputtoIdx].y + 50 + (desColIdx * 10) //100+(desColIdx*10);
                break;
              }
            }
            Connectionrow["fromConnector"] = 'r' + nic
                        Connectionrow["toConnector"] = 'l' + nic
                        Connectionrow["from"] = diagramconnection[conId].from;
                        Connectionrow["to"] = diagramconnection[conId].to;
                        Connectionrow["frmY"] = FromY
                        Connectionrow["toY"] = ToY
                        Connectionrow["text"] = diagramconnection[conId].text;
                        Connectionrow["ftname"] = diagramconnection[conId].fname;
                        Connectionrow["ttname"] = diagramconnection[conId].tname;
                        Connectionrow["type"] = "polyline";
                        if (FromX) if (FromY) if (ToX) if (ToY) {
                            MasterConnection.push(Connectionrow);
                            nic++;
                        }
          }
        

          if (diagramdata.length > MasterDiagramData.length) {
            for (let idd = 0; idd < diagramdata.length; idd++) {
                let ntabid;
                //console.log(MasterDiagramData.length,"eswar - MasterDiagramData");
                if (MasterDiagramData.length > 0)
                    for (let idn = 0; idn < MasterDiagramData.length; idn++) {

                        if (MasterDiagramData[idn].id === diagramdata[idd].id) {
                            ntabid = true;
                            //console.log(ntabid,MasterDiagramData[idn].id,' - ',diagramdata[idd].id,diagramdata[idd].name,"ntabid-if");
                            break;
                        }
                        else {
                            ntabid = false;
                            //  console.log(ntabid,MasterDiagramData[idn].id,' - ',diagramdata[idd].id,diagramdata[idd].name,"ntabid-else");
                        }

                    }

              
                if (!ntabid) {
                    xaxis += 250;
                    diagramdata[idd].x = xaxis; diagramdata[idd].y = yaxis;
                    MasterDiagramData.push(diagramdata[idd]);
                }
            }
        }

        }
        localStorage.setItem("master-value",JSON.stringify(MasterDiagramData));
        localStorage.setItem("master-connection",JSON.stringify(MasterConnection));
        if(diagramName==="physicaldiagram")
        $("#" + diagramName + window.localStorage.getItem('ser_CardId')).kendoDiagram({
          dataSource: {
            data: MasterDiagramData,
            schema: {
              model: {
                id: "id"
              }
            }
          },
          connectionsDataSource: {
            data: MasterConnection
          },
          layout: null,
          shapeDefaults: {
          visual:visualTemplate,
          connectors:[{
            name: 'r0',
            position: function (shape) {
             // console.log(MasterConnection,'MasterConnection');
                let hasResult = 0;
                //console.log(diagramName,'diagramName');
                if(diagramName==="physicaldiagram")
                for (var p = 0; p < MasterConnection.length; p++) {
                    //console.log(p,'p-r0')
                    //console.log(shape,'-',MasterConnection,'shape')

                    if (MasterConnection[p].ftname === shape.dataItem.name) {
                        //console.log(shape.connectors.length);
                      
                        for (var q = 0; q < shape.connectors.length; q++) {
                           // console.log(shape, 'q', q, 'shape');
                            if (shape.connectors[q].connections[0])
                           // console.log(shape.connectors[q].connections[0].dataItem.fromConnector , 'r0' , MasterConnection[p].fromConnector,'r0');
                               if(shape.connectors[q].connections[0]) 
                            if (shape.connectors[q].connections[0].dataItem.fromConnector === 'r0' && MasterConnection[p].fromConnector==='r0') {
                                    var Resulty;
                                    for (var coId = 0; coId < shape.dataItem.columns.length; coId++) {
                                        //console.log(shape.dataItem.name,MasterConnection[p].ftname, shape.dataItem.name,MasterConnection[p].text , shape.dataItem.columns[coId].name,'eswar');
                                        if (MasterConnection[p].text === shape.dataItem.columns[coId].name) {
                                            hasResult = 1;
                                           // console.log(shape._bounds.y,'-',coId,'r0 shape._bounds.y + 50 + (coId + 1 * 10)');
                                            //console.log((coId + 1) * 10,"coId + 1 * 10")
                                            Resulty = shape._bounds.y + 45 + ((coId + 1) * 10);
                                           
                                            break;
                                        }
                                    }
                                   //console.log(Resulty, 'Resulty r0');
                                    let value = shape.bounds().right();
                                    value.y = Resulty;
                                    return shape._transformPoint(value);
                                }                                              
                        }                                            
                    }
                }
                //console.log(hasResult,'hasResult');
                if (hasResult === 0) {
                    return shape._transformPoint(shape.bounds().right());
                }
                else
                {

                }
            }
            
        },
        {
            name: 'r1',
            position: function (shape) {
                let hasResult = 0;
                if(diagramName==="physicaldiagram")
                for (var p = 0; p < MasterConnection.length; p++) {
                    //console.log(p,'p')
                    if (MasterConnection[p].ftname === shape.dataItem.name) {
                        //console.log(shape.connectors.length);
                      
                        for (var q = 0; q < shape.connectors.length; q++) {
                           // console.log(shape, 'q', q, 'shape');
                            if (shape.connectors[q].connections[0])
                            //console.log(shape.connectors[q].connections[0].dataItem.fromConnector , 'r1' , MasterConnection[p].fromConnector,'r1');
                               if(shape.connectors[q].connections[0]) 
                            if (shape.connectors[q].connections[0].dataItem.fromConnector === 'r1' && MasterConnection[p].fromConnector==='r1') {
                                    var Resulty;
                                    for (var coId = 0; coId < shape.dataItem.columns.length; coId++) {
                                       // console.log(shape.dataItem.name,MasterConnection[p].ftname, shape.dataItem.name,MasterConnection[p].text , shape.dataItem.columns[coId].name,'eswar');
                                        if (MasterConnection[p].text === shape.dataItem.columns[coId].name) {
                                            hasResult = 1;
                                           // console.log(shape._bounds.y,'-',coId,'shape._bounds.y + 50 + (coId + 1 * 10)');
                                            Resulty = shape._bounds.y + 45 + ((coId + 1 )* 10);
                                            break;
                                        }
                                    }
                                    //console.log(Resulty, 'Resulty r1');
                                    let value = shape.bounds().right();
                                    value.y = Resulty;
                                    return shape._transformPoint(value);
                                }                                              
                        }                                            
                    }
                }
                if (hasResult === 0) {
                    return shape._transformPoint(shape.bounds().right());
                }
                else
                {
                  
                }
            }
            
        },
        {
            name: 'r2',
            position: function (shape) {
                let hasResult = 0;
                if(diagramName==="physicaldiagram")
                for (var p = 0; p < MasterConnection.length; p++) {
                    //console.log(p,'p')
                    if (MasterConnection[p].ftname === shape.dataItem.name) {
                       // console.log(shape.connectors.length);
                      
                        for (var q = 0; q < shape.connectors.length; q++) {
                           // console.log(shape, 'q', q, 'shape');
                            if (shape.connectors[q].connections[0])
                            //console.log(shape.connectors[q].connections[0].dataItem.fromConnector , 'r1' , MasterConnection[p].fromConnector,'r1');
                               if(shape.connectors[q].connections[0]) 
                            if (shape.connectors[q].connections[0].dataItem.fromConnector === 'r2' && MasterConnection[p].fromConnector==='r2') {
                                    var Resulty;
                                    for (var coId = 0; coId < shape.dataItem.columns.length; coId++) {
                                        //console.log(shape.dataItem.name,MasterConnection[p].ftname, shape.dataItem.name,MasterConnection[p].text , shape.dataItem.columns[coId].name,'eswar');
                                        if (MasterConnection[p].text === shape.dataItem.columns[coId].name) {
                                            hasResult = 1;
                                            //console.log(shape._bounds.y,'-',coId,'r0 shape._bounds.y + 50 + (coId + 1 * 10)');
                                            //console.log((coId + 1) * 10,"coId + 1 * 10")
                                            Resulty = shape._bounds.y + 45 + ((coId + 1) * 10);
                                           
                                            break;
                                        }
                                    }
                                   // console.log(Resulty, 'Resulty r0');
                                    let value = shape.bounds().right();
                                    value.y = Resulty;
                                    return shape._transformPoint(value);
                                }                                              
                        }                                            
                    }
                }
                if (hasResult === 0) {
                    return shape._transformPoint(shape.bounds().right());
                }
            }
            
        },
        {
            name: 'r3',
            position: function (shape) {
                let hasResult = 0;
                if(diagramName==="physicaldiagram")
                for (var p = 0; p < MasterConnection.length; p++) {
                    //console.log(p,'p')
                    if (MasterConnection[p].ftname === shape.dataItem.name) {
                       // console.log(shape.connectors.length);
                        for (var q = 0; q < shape.connectors.length; q++) {
                           // console.log(shape, 'q', q, 'shape');
                            if (shape.connectors[q].connections[0])
                            //console.log(shape.connectors[q].connections[0].dataItem.fromConnector , 'r1' , MasterConnection[p].fromConnector,'r1');
                               if(shape.connectors[q].connections[0]) 
                            if (shape.connectors[q].connections[0].dataItem.fromConnector === 'r3' && MasterConnection[p].fromConnector==='r3') {
                                    var Resulty;
                                    for (var coId = 0; coId < shape.dataItem.columns.length; coId++) {
                                        //console.log(shape.dataItem.name,MasterConnection[p].ftname, shape.dataItem.name,MasterConnection[p].text , shape.dataItem.columns[coId].name,'eswar');
                                        if (MasterConnection[p].text === shape.dataItem.columns[coId].name) {
                                            hasResult = 1;
                                            //console.log(shape._bounds.y,'-',coId,'r0 shape._bounds.y + 50 + (coId + 1 * 10)');
                                            //console.log((coId + 1) * 10,"coId + 1 * 10")
                                            Resulty = shape._bounds.y + 55 + ((coId + 1) * 10);
                                           
                                            break;
                                        }
                                    }
                                   // console.log(Resulty, 'Resulty r0');
                                    let value = shape.bounds().right();
                                    value.y = Resulty;
                                    return shape._transformPoint(value);
                                }                                              
                        }                                            
                    }
                }
                if (hasResult === 0) {
                    return shape._transformPoint(shape.bounds().right());
                }
            }
            
        },
        {
            name: 'r4',
            position: function (shape) {
                let hasResult = 0;
                if(diagramName==="physicaldiagram")
                for (var p = 0; p < MasterConnection.length; p++) {
                    //console.log(p,'p')
                    if (MasterConnection[p].ftname === shape.dataItem.name) {
                      // if(false)
                      // {
                       // console.log(shape.connectors.length);
                      
                        for (var q = 0; q < shape.connectors.length; q++) {
                           // console.log(shape, 'q', q, 'shape');
                            if (shape.connectors[q].connections[0])
                            //console.log(shape.connectors[q].connections[0].dataItem.fromConnector , 'r1' , MasterConnection[p].fromConnector,'r1');
                               if(shape.connectors[q].connections[0]) 
                            if (shape.connectors[q].connections[0].dataItem.fromConnector === 'r4' && MasterConnection[p].fromConnector==='r4') {
                                    var Resulty;
                                    for (var coId = 0; coId < shape.dataItem.columns.length; coId++) {
                                        //console.log(shape.dataItem.name,MasterConnection[p].ftname, shape.dataItem.name,MasterConnection[p].text , shape.dataItem.columns[coId].name,'eswar');
                                        if (MasterConnection[p].text === shape.dataItem.columns[coId].name) {
                                            hasResult = 1;
                                            //console.log(shape._bounds.y,'-',coId,'r0 shape._bounds.y + 50 + (coId + 1 * 10)');
                                            //console.log((coId + 1) * 10,"coId + 1 * 10")
                                            Resulty = shape._bounds.y + 55 + ((coId + 1) * 10);
                                           
                                            break;
                                        }
                                    }
                                   // console.log(Resulty, 'Resulty r0');
                                    let value = shape.bounds().right();
                                    value.y = Resulty;
                                    return shape._transformPoint(value);
                                }                                              
                        }                                            
                    }
                }
                if (hasResult === 0) {
                    return shape._transformPoint(shape.bounds().right());
                }
            }
            
        },
        {
            name: 'l0',
            position: function (shape) {
                let hasResult = 0;
                if(diagramName==="physicaldiagram")
                for (var p = 0; p < MasterConnection.length; p++) {
                    //console.log(p,'p')
                    if (MasterConnection[p].ttname === shape.dataItem.name) {
                       // console.log(shape.connectors.length);
                      
                        for (var q = 0; q < shape.connectors.length; q++) {
                           // console.log(shape, 'q', q, 'shape');
                            if (shape.connectors[q].connections[0])
                            //console.log(shape.connectors[q].connections[0].dataItem.toConnector , 'r1' , MasterConnection[p].toConnector,'r1');
                               if(shape.connectors[q].connections[0]) 
                            if (shape.connectors[q].connections[0].dataItem.toConnector === 'l0' && MasterConnection[p].toConnector==='l0') {
                                    var Resulty;
                                    for (var coId = 0; coId < shape.dataItem.columns.length; coId++) {
                                        //console.log(shape.dataItem.name,MasterConnection[p].ftname, shape.dataItem.name,MasterConnection[p].text , shape.dataItem.columns[coId].name,'eswar');
                                        if (MasterConnection[p].text === shape.dataItem.columns[coId].name) {
                                            hasResult = 1;
                                            //console.log(shape._bounds.y,'-',coId,'r0 shape._bounds.y + 50 + (coId + 1 * 10)');
                                            //console.log((coId + 1) * 10,"coId + 1 * 10")
                                            Resulty = shape._bounds.y + 45 + ((coId + 1) * 10);
                                           
                                            break;
                                        }
                                    }
                                   // console.log(Resulty, 'Resulty r0');
                                    let value = shape.bounds().left();
                                    value.y = Resulty;
                                    return shape._transformPoint(value);
                                }                                              
                        }                                            
                    }
                }
                if (hasResult === 0) {
                    return shape._transformPoint(shape.bounds().left());
                }
            }
            
        },
        {
            name: 'l1',
            position: function (shape) {
                let hasResult = 0;
                if(diagramName==="physicaldiagram")
                for (var p = 0; p < MasterConnection.length; p++) {
                    //console.log(p,'p')
                    if (MasterConnection[p].ttname === shape.dataItem.name) {
                       // console.log(shape.connectors.length);
                      
                        for (var q = 0; q < shape.connectors.length; q++) {
                           // console.log(shape, 'q', q, 'shape');
                            if (shape.connectors[q].connections[0])
                            //console.log(shape.connectors[q].connections[0].dataItem.toConnector , 'r1' , MasterConnection[p].toConnector,'r1');
                               if(shape.connectors[q].connections[0]) 
                            if (shape.connectors[q].connections[0].dataItem.toConnector === 'l1' && MasterConnection[p].toConnector==='l1') {
                                    var Resulty;
                                    for (var coId = 0; coId < shape.dataItem.columns.length; coId++) {
                                        //console.log(shape.dataItem.name,MasterConnection[p].ftname, shape.dataItem.name,MasterConnection[p].text , shape.dataItem.columns[coId].name,'eswar');
                                        if (MasterConnection[p].text === shape.dataItem.columns[coId].name) {
                                            hasResult = 1;
                                            //console.log(shape._bounds.y,'-',coId,'r0 shape._bounds.y + 50 + (coId + 1 * 10)');
                                            //console.log((coId + 1) * 10,"coId + 1 * 10")
                                            Resulty = shape._bounds.y + 45 + ((coId + 1) * 10);
                                           
                                            break;
                                        }
                                    }
                                   // console.log(Resulty, 'Resulty r0');
                                    let value = shape.bounds().left();
                                    value.y = Resulty;
                                    return shape._transformPoint(value);
                                }                                              
                        }                                            
                    }
                }
                if (hasResult === 0) {
                    return shape._transformPoint(shape.bounds().left());
                }
            }
            
        },
        {
            name: 'l2',
            position: function (shape) {
                let hasResult = 0;
                if(diagramName==="physicaldiagram")
                for (var p = 0; p < MasterConnection.length; p++) {
                    //console.log(p,'p',MasterConnection[p].ttname , shape.dataItem.name)
                    if (MasterConnection[p].ttname === shape.dataItem.name) {
                        //console.log(shape.connectors.length);
                      
                        for (var q = 0; q < shape.connectors.length; q++) {
                           // console.log(shape, 'q', q, 'shape');
                            if (shape.connectors[q].connections[0])
                            //console.log(shape.connectors[q].connections[0].dataItem.toConnector , 'r1' , MasterConnection[p].toConnector,'r1');
                               if(shape.connectors[q].connections[0]) 
                            if (shape.connectors[q].connections[0].dataItem.toConnector === 'l2' && MasterConnection[p].toConnector==='l2') {
                                    var Resulty;
                                    for (var coId = 0; coId < shape.dataItem.columns.length; coId++) {
                                       // console.log(shape.dataItem.name,MasterConnection[p].ftname, shape.dataItem.name,MasterConnection[p].text , shape.dataItem.columns[coId].name,'eswar');
                                        if (MasterConnection[p].text === shape.dataItem.columns[coId].name) {
                                            hasResult = 1;
                                            //console.log(shape._bounds.y,'-',coId,'r0 shape._bounds.y + 50 + (coId + 1 * 10)');
                                            //console.log((coId + 1) * 10,"coId + 1 * 10")
                                            Resulty = shape._bounds.y + 45 + ((coId + 1) * 10);
                                           
                                            break;
                                        }
                                    }
                                    //console.log(Resulty, 'Resulty l2');
                                    let value = shape.bounds().left();
                                    value.y = Resulty;
                                    return shape._transformPoint(value);
                                }                                              
                        }                                            
                    }
                }
                if (hasResult === 0) {
                    return shape._transformPoint(shape.bounds().left());
                }
            }
            
        },
        {
            name: 'l3',
            position: function (shape) {
                let hasResult = 0;
                if(diagramName==="physicaldiagram")
                for (var p = 0; p < MasterConnection.length; p++) {
                    //console.log(p,'p')
                    if (MasterConnection[p].ttname === shape.dataItem.name) {
                       // console.log(shape.connectors.length);
                      
                        for (var q = 0; q < shape.connectors.length; q++) {
                           // console.log(shape, 'q', q, 'shape');
                            if (shape.connectors[q].connections[0])
                            //console.log(shape.connectors[q].connections[0].dataItem.toConnector , 'r1' , MasterConnection[p].toConnector,'r1');
                               if(shape.connectors[q].connections[0]) 
                            if (shape.connectors[q].connections[0].dataItem.toConnector === 'l3' && MasterConnection[p].toConnector==='l3') {
                                    var Resulty;
                                    for (var coId = 0; coId < shape.dataItem.columns.length; coId++) {
                                        //console.log(shape.dataItem.name,MasterConnection[p].ftname, shape.dataItem.name,MasterConnection[p].text , shape.dataItem.columns[coId].name,'eswar');
                                        if (MasterConnection[p].text === shape.dataItem.columns[coId].name) {
                                            hasResult = 1;
                                            //console.log(shape._bounds.y,'-',coId,'r0 shape._bounds.y + 50 + (coId + 1 * 10)');
                                            //console.log((coId + 1) * 10,"coId + 1 * 10")
                                            Resulty = shape._bounds.y + 45 + ((coId + 1) * 10);
                                           
                                            break;
                                        }
                                    }
                                   // console.log(Resulty, 'Resulty r0');
                                    let value = shape.bounds().left();
                                    value.y = Resulty;
                                    return shape._transformPoint(value);
                                }                                              
                        }                                            
                    }
                }
                if (hasResult === 0) {
                    return shape._transformPoint(shape.bounds().left());
                }
            }
            
        },
        {
            name: 'l4',
            position: function (shape) {
                let hasResult = 0;
                if(diagramName==="physicaldiagram")
                for (var p = 0; p < MasterConnection.length; p++) {
                    //console.log(p,'p')
                    if (MasterConnection[p].ttname === shape.dataItem.name) {
                      // if(false)
                      // {
                       // console.log(shape.connectors.length);
                      
                        for (var q = 0; q < shape.connectors.length; q++) {
                           // console.log(shape, 'q', q, 'shape');
                            if (shape.connectors[q].connections[0])
                            //console.log(shape.connectors[q].connections[0].dataItem.toConnector , 'r1' , MasterConnection[p].toConnector,'r1');
                               if(shape.connectors[q].connections[0]) 
                            if (shape.connectors[q].connections[0].dataItem.toConnector === 'l4' && MasterConnection[p].toConnector==='l4') {
                                    var Resulty;
                                    for (var coId = 0; coId < shape.dataItem.columns.length; coId++) {
                                        //console.log(shape.dataItem.name,MasterConnection[p].ftname, shape.dataItem.name,MasterConnection[p].text , shape.dataItem.columns[coId].name,'eswar');
                                        if (MasterConnection[p].text === shape.dataItem.columns[coId].name) {
                                            hasResult = 1;
                                            //console.log(shape._bounds.y,'-',coId,'r0 shape._bounds.y + 50 + (coId + 1 * 10)');
                                            //console.log((coId + 1) * 10,"coId + 1 * 10")
                                            Resulty = shape._bounds.y + 45 + ((coId + 1) * 10);
                                           
                                            break;
                                        }
                                    }
                                   // console.log(Resulty, 'Resulty r0');
                                    let value = shape.bounds().left();
                                    value.y = Resulty;
                                    return shape._transformPoint(value);
                                }                                              
                        }                                            
                    }
                }
                if (hasResult === 0) {
                    return shape._transformPoint(shape.bounds().left());
                }
            }
            
        }
      ]
          },
          //selectable: false,
          editable: {
            tools: false
          },
         
         
          connectionDefaults: {
            startCap: {
              type: "FilledCircle",
              fill: "#228B22"
            },
            endCap: {
              type: "ArrowEnd",
              fill: "#228B22"
            },
            stroke: {
              color: "#2713DF",
              width: 2
            },
            content: {
              template: "#:dataItem.text#",
              fontSize: 9,
              color: "#FF0000"
            }
          },

        });
        else
        $("#" + diagramName + window.localStorage.getItem('ser_CardId')).kendoDiagram({
          dataSource: {
            data: MasterDiagramData,
            schema: {
              model: {
                id: "id"
              }
            }
          },
          connectionsDataSource: {
            data: MasterConnection
          },
          layout: null,
          shapeDefaults: {
          visual:visualTemplate
          },
          editable: {
            tools: false
          },
          connectionDefaults: {
            startCap: {
              type: "FilledCircle",
              fill: "#228B22"
            },
            endCap: {
              type: "ArrowEnd",
              fill: "#228B22"
            },
            stroke: {
              color: "#2713DF",
              width: 2
            },
           
            content: {
              template: "#:dataItem.text#",
              fontSize: 9,
              color: "#FF0000"
             
            }
          },

        });
      })


  }

  componentDidUpdate() {
    if (this.state.visible === true && this.state.collapse === true) {
      var visualTemplate = (options) => {
        var dataviz = kendo.dataviz;
        var g = new dataviz.diagram.Group();
        var dataItem = options.dataItem;
        var currY = 30

        g.append(new dataviz.diagram.Rectangle({
          width: 150,
          height: 30,
          stroke: {
            width: 1,
            color: "#3399dd"
          },
          fill: {
            color: "#3399dd"
          }
        }));

        g.append(new dataviz.diagram.TextBlock({
          text: dataItem.name.toUpperCase(),
          // text: <i class="fa fa-bars"></i>,
          x: 40,
          y: 10,
          fill: "#fff",
          fontSize: 10,
        }));

        for (var i = 0; i < dataItem.columns.length; i++) {
          currY += 15;
          g.append(new dataviz.diagram.TextBlock({
            text: dataItem.columns[i].name,
            x: 5,
            y: currY,
            fill: "#000",
            fontSize: 10
          }));

          g.append(new dataviz.diagram.TextBlock({
            text: dataItem.columns[i].type,
            x: 100,
            y: currY,
            fill: "#000",
            fontSize: 10
          }));
        }
        g.append(new dataviz.diagram.Rectangle({
          width: 150,
          height: dataItem.columns.length + currY + 15,
          stroke: {
            width: 1,
            color: "#3399dd"
          },
          fill: {
          }
        }));
        $("#loading").find("div.k-loading-mask").remove();
        return g;
      }
      var $cell = $('.card');
      //open and close card when clicked on card
      $cell.find('.js-expander').click(function () {
        var $thisCell = $(this).closest('.card');
        if ($thisCell.hasClass('is-collapsed')) {
          $cell.not($thisCell).removeClass('is-expanded').addClass('is-collapsed').addClass('is-inactive');
          $thisCell.removeClass('is-collapsed').addClass('is-expanded');
          //if ($cell.not($thisCell).hasClass('is-inactive')) {
          var id = $thisCell[0].id;
          var value = $("#" + id + " :input");
          const user = {
            path: value[0].value,
            //caption: value[0].name,
            server_id: value[0].id
          }
          window.localStorage.setItem("ser_id", user.server_id)
          window.localStorage.setItem("ser_CardId", $thisCell[0].id)
          // window.localStorage.setItem("ser_caption", user.caption)
          window.localStorage.setItem("ser_path", user.path)
          $("#loading").prepend("<div class='k-loading-mask' style='width:100%;height:100%'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'><div class='k-loading-color'></div></div></div>");
          getlogicalDatadigram(user, "logicaldiagram")
            .then((response) => {
              let diagramdata = response.data;
              let diagramconnection = [];
              var input = diagramdata;
              
              for (var i = 0; i < input.length; i++) {
                if (input[i].to) {
                  var connectto = input[i].to.length;
                  for (var j = 0; j < connectto; j++) {
                    var row = {}
                    row["from"] = input[i].id;
                    row["to"] = input[i].to[j];
                    row["text"] = "";
                    diagramconnection.push(row);
                  }
                }
              }

              let MasterDiagramData = [];
              let xaxis = 50, yaxis = 50;
              let order = 1;
              let Layer = [];
              const map = new Map();
              const diagramdataMapping = new Map();

              for (var dc = 0; dc < diagramconnection.length; dc++) {
                if (!map.has(diagramconnection[dc].from)) {
                  let inputIdx;
                  for (var idx = 0; idx < diagramdata.length; idx++) {
                    if (diagramdata[idx].id === diagramconnection[dc].from) {
                      inputIdx = idx;
                    }
                  }
                  if (!diagramdataMapping.has(diagramdata[inputIdx].id)) {
                    map.set(diagramdata[inputIdx].id, true);
                    diagramdataMapping.set(diagramdata[inputIdx].id, true);
                    diagramdata[inputIdx].x = xaxis; diagramdata[inputIdx].y = yaxis;
                    MasterDiagramData.push(diagramdata[inputIdx]);
                    Layer.push({ layer: order, id: diagramdata[inputIdx].id, toid: "", x: xaxis, y: yaxis });

                  }
                  if (diagramdata[inputIdx].to.length) {
                    let tolength = diagramdata[inputIdx].to.length;
                    order++; xaxis += 300;
                    for (let toIdx = 0; toIdx < tolength; toIdx++) {
                      let inputtoIdx;
                      for (let idx = 0; idx < diagramdata.length; idx++) {

                        if (diagramdata[idx].id === diagramdata[inputIdx].to[toIdx]) {
                          inputtoIdx = idx;
                        }
                      }

                      if (!diagramdataMapping.has(diagramdata[inputtoIdx].id)) {
                        for (let di = 0; di < Layer.length; di++) {
                          if (diagramdata[inputIdx].id === Layer[di].id) {
                            for (let diq = 0; diq < Layer.length; diq++) {
                              if (Layer[di].layer + 1 === Layer[diq].layer) {
                                xaxis = Layer[diq].x;
                                yaxis = Layer[diq].y;
                              }
                            }
                          }
                        }
                        diagramdataMapping.set(diagramdata[inputtoIdx].id, true);
                        diagramdata[inputtoIdx].x = xaxis; diagramdata[inputtoIdx].y = yaxis;
                        MasterDiagramData.push(diagramdata[inputtoIdx]);
                        yaxis += (MasterDiagramData[MasterDiagramData.length - 1].columns.length) * 15 + 75;
                        Layer.push({ layer: order, id: diagramdata[inputIdx].to[toIdx], toid: diagramdata[inputtoIdx].id, x: xaxis, y: yaxis })
                      }
                    }
                    yaxis = 50;
                  }

                }
              }

              if (diagramdata.length > MasterDiagramData.length) {
                for (let idd = 0; idd < diagramdata.length; idd++) {
                  let ntabid;
                  if (MasterDiagramData.length > 0)
                    for (let idn = 0; idn < MasterDiagramData.length; idn++) {
                      if (MasterDiagramData[idn].id === diagramdata[idd].id) {
                        ntabid = true;
                        break;
                      }
                      else {
                        ntabid = false;
                      }

                    }
                  if (!ntabid) {
                    xaxis += 300;
                    diagramdata[idd].x = xaxis; diagramdata[idd].y = yaxis;

                    MasterDiagramData.push(diagramdata[idd]);
                  }
                  
                }
              }


              $("#loading").find("div.k-loading-mask").remove();
              $("#logicaldiagram" + $thisCell[0].id).kendoDiagram({
                dataSource: {
                  data: MasterDiagramData,
                  schema: {
                    model: {
                      id: "id"
                    }
                  }
                },
                connectionsDataSource: {
                  data: diagramconnection
                },
                layout: null,
                shapeDefaults: {
                  visual: visualTemplate
                },
                connectors:[{}],
                //selectable: false,
                editable: {
                  tools: false
                },
                connectionDefaults: {
                  startCap: {
                    type: "FilledCircle",
                    fill: "#228B22"
                  },
                  endCap: {
                    type: "ArrowEnd",
                    fill: "#228B22"
                  },
                  stroke: {
                    color: "#2713DF",
                    width: 2
                  },
                  content: {
                    template: "#:dataItem.text#",
                    fontSize: 7,
                    color: "#FF0000"
                  }
                },

              });
            })
            .catch((error) => {
              $("#loading").find("div.k-loading-mask").remove();
            });
       
        }
        else {
          $thisCell.removeClass('is-expanded').addClass('is-collapsed');
          $cell.not($thisCell).removeClass('is-inactive');
        }
      });

      //close card when click on cross
      $cell.find('.js-collapser').click(function () {
        var $thisCell = $(this).closest('.card');
        $thisCell.removeClass('is-expanded').addClass('is-collapsed');
        $cell.not($thisCell).removeClass('is-inactive');

      });
    }
  }

  handleKeywordsChange(e) {
    var $cell = $('.card');
    if (e.target.value != null) {

      var $thisCell = $(this).closest('.card');
      $cell.not($thisCell).removeClass('is-inactive');
      const tablenames = this.state.masterdata;
      const startsWithN = tablenames.filter((table) => table.caption.toUpperCase().startsWith(e.target.value.toUpperCase()));
      // const startsWithN = tablenames.filter((table) => table.caption.startsWith(e.target.value));
      this.setState({ filterdata: startsWithN });
    }

    $cell.find('.js-collapser').click(function () {
      var $thisCell = $(this).closest('.card');
      $thisCell.removeClass('is-expanded').addClass('is-collapsed');
      $cell.not.removeClass('is-inactive');


    });
      

  }
  btnclick = () => {
    this.setState({
      visible: false,
      collapse: true,
      selected: 0
    })
  }
  render() {

    return (
      <div>
        <Segment>
          <h3>Views</h3>

        </Segment>
        <Segment>

          <div id="loading">
          </div>
          <div className={"fltr-div-srch"} >
            <Input
              onChange={this.handleKeywordsChange}
              name="txtsearch" type="text" icon='search' placeholder='Search...' />
          </div>
          <div class='wrapper'><div class='cards'>
            {
              this.state.filterdata.map((user, i) =>
                <div id={i} key={i} title={user.caption} class=' card [ is-collapsed ] '>
                  <div class='card__inner [ js-expander ]' onClick={this.btnclick}>
                    <input type="hidden" id={user.server_id} name={user.caption} value={user.path}></input>
                   
                    <span>{user.caption}</span>
                    <i class='fa fa-folder-o'></i>
                  </div>
                  <div class='card__expander'>
                    <TabStrip selected={this.state.selected} onSelect={this.handleSelect}>
                      <TabStripTab title="Logical View">
                        <div id={"logicaldiagram" + i} style={{ fontSize: "6px" }} ></div>

                      </TabStripTab>
                      <TabStripTab title="Physical View">
                        <div id={"physicaldiagram" + i} style={{ fontSize: "6px" }} ></div>
                      </TabStripTab>

                    </TabStrip>
                    <i style={{ color: 'red' }} class='fa fa-close [ js-collapser ]'></i>
                  </div>
                </div>)
            }
          </div>
          </div>

        </Segment>
      </div>
    );
  }
}
export default Logicalview;