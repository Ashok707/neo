import React, { Component } from 'react';
import { TreeView } from '@progress/kendo-react-treeview';
import { Grid, GridColumn as Column, GridCell } from '@progress/kendo-react-grid';
import { Segment, Input, Header, Button, Breadcrumb } from "semantic-ui-react";
import { DropDownList } from '@progress/kendo-react-dropdowns';
import { reportdetails, convertReports, getnodeData, LoadsubItems } from '../../services/reportService';
import { Dialog, DialogActionsBar } from '@progress/kendo-react-dialogs';
import '../../styles/index.css';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

class node extends Component {
    lastSelectedIndex = 0;
    constructor(props) {
        super(props);
        this.state = {
            value: { text: 'Neo', id: 1 },
            servername: props.history.servername,
            selItemArray: [],
            data: [],
            filterdata: [],
            arrayserverpath: [],
            visible: false,
            showing: false,
            showing_1: false,
            selItemArrayForChild: [],
            previousSelected: []
        };
        this.pageChange = this.pageChange.bind(this);
        this.selectionChange = this.selectionChange.bind(this);
        this.toggleDialog = this.toggleDialog.bind(this);
        const remove = this.remove.bind(this);
       
        class MyCommandCell extends GridCell {
            render() {
                return !this.props.dataItem.inEdit ?
                    (
                        <td>

                            <button  
                            //style={{width:'100px', color:"white", backgroundColor:'red'}}
                            class="btn"
                                onClick={(e) =>
                                    confirmAlert({
                                        message: 'Are you want to remove this?',
                                        buttons: [
                                          {
                                            label: 'Yes',
                                            onClick: () => remove(this.props.dataItem)
                                          },
                                          {
                                            label: 'No',
                                            onClick: () => console.log()
                                          }
                                        ]
                                      })
                                    }
                                 
                            ><i class="fa fa-trash-o"></i>
                            </button>

                        </td>
                    ) : (
                        <td>

                        </td>
                    );
            }
        }
        this.CommandCell = MyCommandCell;
    }
  
    toggleDialog() {
        this.setState({
            visible: !this.state.visible
        });
    }
    remove(dataItem) {
        dataItem.inEdit = undefined;
        this.update(this.state.selItemArrayForChild, dataItem, true);
        this.setState({
            selItemArrayForChild: this.state.selItemArrayForChild.slice()
        });
    }

    update(selItemArrayForChild, item, remove) {
        let updated;
        let index = selItemArrayForChild.findIndex(p => p === item || (item.name && p.name) === item.name);

        if (index >= 0) {
            updated = Object.assign({}, item);
            selItemArrayForChild[index] = updated;
        }
        if (remove) {
            selItemArrayForChild = selItemArrayForChild.splice(index, 1);
        }
        return selItemArrayForChild[index];
    }

    selectionChange = (event) => {
        event.dataItem.selected = !event.dataItem.selected;
        this.forceUpdate()
        if (event.dataItem.selected) {
            this.setState({
                showing_1: true
            })
            let last = event.dataItem.path;
            let name = event.dataItem.caption
            const userdata = {
                path: last
            }
            reportdetails(userdata)
                .then(response => {
                    var newItemInput = response.data.column_count
                    const obj = { 'count': newItemInput, "caption": name, "path": last };
                    this.state.selItemArrayForChild.push(obj); // Push the object
                    this.forceUpdate()
                    this.setState({
                        showing_1: false
                    })
                })
            window.scrollTo(0, document.body.scrollHeight);
        }
        else {
            var dataItem = event.dataItem
            this.remove(dataItem)
            this.forceUpdate()
            
        }
    }

    pageChange(event) {
        var id = localStorage.getItem('ServerID')
        for (var i = 0; i < this.state.selItemArrayForChild.length; i++) {
            const userdata = [{
                path: this.state.selItemArrayForChild[i].path,
                caption: this.state.selItemArrayForChild[i].caption,
                server_id: id
            }]
            convertReports(userdata)
                .then(response => {
                    if (response.data === true) {
                        let path = `/neoreports`;
                        this.props.history.push(path);
                    }
                })
        }
    }

    headerSelectionChange = (event) => {
        const checked = event.syntheticEvent.target.checked;
        this.state.data.forEach(item => item.selected = checked);
        this.forceUpdate();
    }
    handleKeywordsChange = (e) => {
        if (e.target.value != null) {
            var items = this.state.filterdata; 
              if (items !== undefined) {
              //  console.log(this.state.data, 'data')
                if (this.state.filterdata[0].hasChildren === true && this.state.filterdata[0].selected === true) {
                    items = this.state.filterdata[0].items;
                    const startsWithN = items.filter((table) => table.text.toUpperCase().startsWith(e.target.value.toUpperCase()));
                    var flt = this.state.filterdata[0].id.split('/')
                    if (flt.length !== 3) {
                        this.setState({ data: startsWithN });
                    }
                    else {
                        this.setState({
                            showing: true
                        })
                        getnodeData()
                            .then(json => {
                               
                                if (json.data === "") {
                                    let path = `/filter`;
                                    this.props.history.push(path);
                                }
                                else {
                                    const data = json.data.map(({ path, caption, type }) => {
                                        return { text: caption, id: path, type: type, hasChildren: true }
                                    });
                                    this.iconClassName({ data })
                                    this.setState({ data, showing: false, filterdata: data, selItemArray: [] });
                                }
                            });
                    } 

                }
                else if (this.state.filterdata[1].hasChildren === true && this.state.filterdata[1].selected === true) {
                    items = this.state.filterdata[1].items;
                    const startsWithN = items.filter((table) => table.text.toUpperCase().startsWith(e.target.value.toUpperCase()));
                    this.setState({ data: startsWithN });

                }
                else {
                    if (this.state.filterdata[0].hasChildren === true && this.state.filterdata[0].selected === true) {
                        items = this.state.filterdata[0].items;
                        const startsWithN = items.filter((table) => table.text.toUpperCase().startsWith(e.target.value.toUpperCase()));
                        this.setState({ data: startsWithN });  
                  
                     
                    }
                    else if (this.state.filterdata[1].hasChildren === true && this.state.filterdata[1].selected === true) {
                        items = this.state.filterdata[1].items;
                        const startsWithN = items.filter((table) => table.text.toUpperCase().startsWith(e.target.value.toUpperCase()));
                        this.setState({ data: startsWithN }); 
                    
                    }
                    else {
                        if (this.state.filterdata[0].expanded === true && this.state.filterdata[0].hasChildren === false) {
                            items = this.state.filterdata[0].items;
                            console.log("data-else-data[0]");
                            const startsWithN = items.filter((table) => table.text.toUpperCase().startsWith(e.target.value.toUpperCase()));
                            this.setState({ data: startsWithN }); 
                          
                        }
                        else if (this.state.filterdata[1].expanded === true && this.state.filterdata[1].hasChildren === false) {
                            items = this.state.filterdata[0].items;
                            const startsWithN = this.state.filterdata[1].items.filter((table) => table.text.toUpperCase().startsWith(e.target.value.toUpperCase()));
                            this.setState({ data: startsWithN }); 
                           
                        }
                        else {
                            console.log(items, "data-else");
                            this.setState({
                                showing: true
                            })
                            getnodeData()
                                .then(json => {
                                    if (json.data === "") {
                                        let path = `/filter`;
                                        this.props.history.push(path);
                                    }
                                    else {
                                        const data = json.data.map(({ path, caption, type }) => {
                                            return { text: caption, id: path, type: type, hasChildren: true }
                                        });
                                        this.iconClassName({ data })
                                        this.setState({ data, showing: false, filterdata: data, selItemArray: [] });
                                    }
                                });
                        }
                    }
                }
            }
            else {
                console.log("data-else1");
                this.setState({ data: this.state.filterdata });   
            }
        }
        else {
            console.log("data-else2");
        }
        if (e.target.value.length === 0) {
            this.setState({ data: this.state.filterdata });
        }
    }

    render() {
        return (
            <div>
                <div className="topmenu-body">
                    <Segment>
                        <Header as='h3'>Choose your Report</Header>
                    </Segment>
                    <Breadcrumb>
                        <Breadcrumb.Section href="/filter">OBI Servers</Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section href="/filter" active>{this.state.servername}</Breadcrumb.Section>
                    </Breadcrumb>
                    <Segment>
                        <Input onChange={this.handleKeywordsChange} name="txtsearch" type="text" icon='search' placeholder='Search Reports' />
                        <div className={"div-tree-top row"}>
                            <div className={"col-sm-5"} >
                                <TreeView
                                    data={this.state.data}
                                    onExpandChange={this.onExpandChange}
                                    onItemClick={this.onExpandChange}
                                    itemRender={props =>
                                        [<span className={this.iconClassName(props.item)} key='0'></span>, props.item.text]
                                    } />
                                {this.state.showing
                                    ? <div className="k-i-loading"></div>
                                    : null
                                }

                            </div>
                            <div className={"col-sm-7"}>
                                <Grid
                                    className={"node-grid-align"}
                                    onSelectionChange={this.selectionChange}
                                    data={this.state.selItemArray}
                                    selectedField="selected">
                                    <Column
                                        field="selected"
                                        width="50px"
                                        onSelectionChange={
                                            this.selectionChange} />
                                    <Column field="caption" title="Report Name" width="500px" />
                                    <br /><br />
                                </Grid>
                                {this.state.showing_1
                                    ? <div class="k-i-loading"></div>
                                    : null
                                }
                            </div>
                        </div>
                    </Segment>
                    <Segment>
                        <Grid
                            className={"node-grid-align"}
                            onSelectionChange={this.selectionChange}
                            data={this.state.selItemArrayForChild}
                            selectedField="selected">
                            <Column field="caption" title="Report Name" width="250px" />
                            <Column field="path" title="Report Path" width="400px" />
                            <Column field="count" title="Columns" width="90px" />
                            <Column cell={this.CommandCell} width="180px" />
                        </Grid>
                    </Segment>
                    <div className={"node-div-al"}>
                        Select Target Type
                                          <DropDownList
                            data={this.Type}
                            textField="text"
                            dataItemKey="id"
                            value={this.state.value}
                            onChange={this.handleChange} />
                        <span className={"node-grid-btn-ct"}>
                            <Button
                                color="teal" type="button"
                                disabled={!this.state.arrayserverpath}
                                onClick={this.toggleDialog}
                                className="btn btn-rad"> convert </Button>
                            {this.state.visible && <Dialog title={"Please confirm"} onClose={this.toggleDialog}>
                                <p style={{ margin: "25px", textAlign: "center" }}>Are you sure you to Convert?</p>
                                <DialogActionsBar>
                                    <button className="k-button" onClick={this.pageChange}>Yes</button>
                                    <button className="k-button" onClick={this.toggleDialog}>No</button>
                                </DialogActionsBar>
                            </Dialog>}
                        </span>
                    </div>
                </div>
            </div>
        );
    }
    iconClassName({ type }) {
        if (type === "Folder") {
            return 'k-icon k-i-folder';
        } else {
            return 'k-icon k-i-file-txt k-i-txt'
        }
    }

    /* Fetch Parent Data on PageLoad*/
    componentDidMount() {
        this.setState({
            showing: true
        })
        getnodeData()
            .then(json => {
                // console.log(json, 'first')
                if (json.data === "") {
                    let path = `/filter`;
                    this.props.history.push(path);
                }
                else {
                    const data = json.data.map(({ path, caption, type }) => {
                        return { text: caption, id: path, type: type, hasChildren: true }
                    });
                    this.iconClassName({ data })
                    this.setState({ data, showing: false, filterdata: data });
                }
            });
    }

    /* Expand Request from Parent Node */
    onExpandChange = (event) => {
        this.setState({
            showing: true
        })
        event.item.selected = !event.item.selected;
        this.setState({
            previousSelected: event.item
        })
        this.forceUpdate()
        var preSelected = this.state.previousSelected
        //  console.log(preSelected)
        const categoryIndex1 = event.itemHierarchicalIndex;
        const { data, category } = this.copyDataAndCategory(categoryIndex1);
        category.expanded = !category.expanded
        this.setState({ data });
        if (category.items === null) {
            this.loadNodes(categoryIndex1, event.item.id, event, preSelected);
        }
        else {
            this.loadNodes(categoryIndex1, event.item.id, event, preSelected);
        }
    }

    /* Load Child Nodes */
    loadNodes(categoryIndex, categoryId, event, preSelected) {

        var pathdata = {
            path: categoryId
        }
        LoadsubItems(pathdata)
            .then(json => {
            //   console.log(json,'data')
                if (json.data === "") {
                    alert("No data Found")  // <-- if no data in folder
                }
                else {
                    var parent = json.data[0].path.split('/')
                    let result = json.data.filter(t => t.type === 'Object');
                    console.log(this.state.selItemArray, 'selitemarray')
                    this.setState({
                        selItemArray: result, filterdata: result
                    })
                    let folder = json.data.filter(t => t.type === 'Folder');
                    this.forceUpdate()
                    const { data, category } = this.copyDataAndCategory(categoryIndex);
                    category.items = folder.map(({ caption, path, type }) => {
                        if (type === "Folder") {
                            return { text: caption, id: path, type: type, hasChildren: true, expanded: false }
                        }
                        else {
                            return { text: caption, id: path, type: type, hasChildren: false, expanded: false }
                        }
                    });
                    this.setState({ data, showing: false, filterdata: data });
                    this.forceUpdate()
                    var slice = event.item.id.split('/')
                    var split = slice[2]
                    if (preSelected.id === undefined) {

                    }
                    else {
                        var id = preSelected.id
                        var slice_one = id.split('/')
                        if (slice_one.length > 2) {
                            var slice_two = slice_one[2]
                        }
                    }
                    if (parent[1] === slice[1]) {
                        console.log(this.state.data, 'console')
                        console.log(this.state.previousSelected, 'console1')
                        if (parent[1] && slice[1] === 'shared') {
                            if (this.state.data.length === 2) {
                            
                            }
                        }
                        else {
                        
                        }
                    }
                    if (slice[1] === 'users') {
                       // let selected_Item = this.state.data[1].items.filter(t => t.selected === true && t.text !== this.state.previousSelected.text)
                    }
                    else {
                        var gg = this.state.data[0].id.split('/')
                        if (gg.length === 2) {
                            var selected_Item = this.state.data[0].items.filter(t => t.selected === true && t.text !== this.state.previousSelected.text)
                            if (selected_Item.length > 0) {
                                if (split === slice_two) {
                                    this.state.data[0].items.find(item => item.text === selected_Item[0].text).selected = true;
                                    this.state.data[0].items.find(item => item.text === selected_Item[0].text).expanded = true;
                                    this.forceUpdate()
                                }
                                else {
                                    this.state.data[0].items.find(item => item.text === selected_Item[0].text).selected = false;
                                    this.state.data[0].items.find(item => item.text === selected_Item[0].text).expanded = false;
                                    this.forceUpdate()
                                }
                            }
                        }
                    }
                }
            })
    }

    /* Bind Child Nodes into Parent Nodes */
    copyDataAndCategory(categoryIndex) {
        var _count = categoryIndex.toString().split('_');

        if (categoryIndex.toString().indexOf('_') === -1) {
            let data = this.state.data.slice();
            let category = data[categoryIndex] = { ...data[categoryIndex] };
            return { data, category };
        }
        else if (_count.length === 2) {
            let data = this.state.data.slice()
            let category = data[_count[0]].items[_count[1]] = { ...data[_count[0]].items[_count[1]] };
            return { data, category };
        }
        else if (_count.length === 3) {
            let data = this.state.data.slice()
            let category = data[_count[0]].items[_count[1]].items[_count[2]] = { ...data[_count[0]].items[_count[1]].items[_count[2]] };
            return { data, category };
        }
        else if (_count.length === 4) {
            let data = this.state.data.slice()
            let category = data[_count[0]].items[_count[1]].items[_count[2]].items[_count[3]] = { ...data[_count[0]].items[_count[1]].items[_count[2]].items[_count[3]] };
            return { data, category };
        }
        else if (_count.length === 5) {
            let data = this.state.data.slice()
            let category = data[_count[0]].items[_count[1]].items[_count[2]].items[_count[3]].items[_count[4]] = { ...data[_count[0]].items[_count[1]].items[_count[2]].items[_count[3]].items[_count[4]] };
            return { data, category };
        }
        else if (_count.length === 6) {
            let data = this.state.data.slice()
            let category = data[_count[0]].items[_count[1]].items[_count[2]].items[_count[3]].items[_count[4]].items[_count[5]] = { ...data[_count[0]].items[_count[1]].items[_count[2]].items[_count[3]].items[_count[4]].items[_count[5]] };
            return { data, category };
        }
        else if (_count.length === 7) {
            let data = this.state.data.slice()
            let category = data[_count[0]].items[_count[1]].items[_count[2]].items[_count[3]].items[_count[4]].items[_count[5]].items[_count[6]] = { ...data[_count[0]].items[_count[1]].items[_count[2]].items[_count[3]].items[_count[4]].items[_count[5]].items[_count[6]] };
            return { data, category };
        }
        else if (_count.length === 8) {
            let data = this.state.data.slice()
            let category = data[_count[0]].items[_count[1]].items[_count[2]].items[_count[3]].items[_count[4]].items[_count[5]].items[_count[6]].items[_count[7]] = { ...data[_count[0]].items[_count[1]].items[_count[2]].items[_count[3]].items[_count[4]].items[_count[5]].items[_count[6]].items[_count[7]] };
            return { data, category };
        }
    }
}

export default node