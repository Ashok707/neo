import React, { Component } from 'react';
import kendo from '@progress/kendo-ui';
import { Grid, GridColumn } from '@progress/kendo-grid-react-wrapper';
import { Segment, Input, Header } from "semantic-ui-react";
import { getneoreportdata, neoreports } from '../../services/reportService';
import $ from 'jquery';
import '../../styles/index.css';
class Reports extends Component {
    count = 0
    constructor(props) {
        super(props);
        this.state = {
            //data:[],
            ServerID: '',
            selItem: '',
            data1: [],
            selItemArray: [],
            gdata: 'No Data Selected',
            path: '',
            ChildGridData: [],
            childVisible: false,
            Apiincr: 0,
            sdata: [],
            showing:false,
            showing_1:false
           
        };

     
        var crudServiceBaseUrl = `${neoreports.path}`
        this.dataSource = new kendo.data.DataSource({
            
            transport: {
                read: {
                    url: crudServiceBaseUrl
                },

                parameterMap: function (options, operation) {
                   
                }
            },
            select: true,
            batch: true,
            pageSize: 10,
            schema: {
                model: {
                    id: "server_id",
                    fields: {
                       
                        path: { editable: false, nullable: true },
                        server_id: { editable: false, nullable: true },
                        caption: { type: "string", validation: { required: true } },
                        created_by: { type: "string", validation: { required: true } },
                        updated_date: { type: "string", validation: { required: true } },
                        conversion_status: { type: "string", validation: { required: true } },
                        overall_status: { type: "string", validation: { required: true } }
                    }

                }
            }
           
        });

        //  this.dataSource = new kendo.data.DataSource({
        //      data: this.state.data.map(
        //     (item,index) => ({RowNumber: index+1,...item }))
        // });
          
        this.handleKeywordsChange = this.handleKeywordsChange.bind(this);
        this.onloadgrid = this.onloadgrid.bind(this);
    }
    handleKeywordsChange(e) {
        var filter = { logic: 'or', filters: [] };
        
        if (e.target.value != null) {
            filter.filters.push({ field: "caption", operator: 'startswith', value: e.target.value })
        }
        this.dataSource.filter(filter);
    }

    onloadgrid(e) {
        this.setState({
            showing:true
        })
        var currentDataItem = e.sender.dataItem(e.sender.select())
        if (currentDataItem !== null) {
            this.forceUpdate();
            localStorage.ServerID = currentDataItem.server_id;
           
            this.setState({ gdata: 'Loading...', ChildGridData: '', ServerID: currentDataItem.server_id });
            this.forceUpdate();
            const user = {
                path: currentDataItem.path
            }

            getneoreportdata(user)
                .then((response) => {
                    if (response.data !== "" && response.data.data !== undefined) {
                        this.setState({ ChildGridData: [] });
                        this.cdataSource = new kendo.data.DataSource({
                            data: []
                        });

                        this.cdataSource.read();
                        this.setState({ gdata: '', cdataSource: null, childVisible: false, data: [], fields: [], title: [] });

                        var input = response.data.data;
                        console.log(input,"input");
                        for (var i = 1; i < input.length; i++) {
                            var row = {}
                            for (var j = 0; j < input[0].length; j++) {
                         
                          var colname = input[0][j].toString().replace(/[^a-zA-Z ]| | |  {2}| | /g,'');
                          
                                if (colname !== null)
                                    row[colname] = input[i][j];
                                else
                                    this.setState({ gdata: 'No Column Name', cdataSource: null, childVisible: false });
                            }
                            this.state.ChildGridData.push(row);
                        }
                        this.cdataSource = new kendo.data.DataSource({
                            data: this.state.ChildGridData,
                            pageSize: 10,
                            headers: false
                        });
                        this.cdataSource.read();

                        this.setState({ gdata: '', childVisible: true, data: null, fields: null, title: null ,showing:false});
                    }
                    else {
                        this.cdataSource = new kendo.data.DataSource({
                            data: []
                        });
                        this.cdataSource.read();
                        this.setState({ gdata: 'No Records', ChildGridData: null, cdataSource: null, childVisible: false, data: null, fields: null, title: null, showing:false });

                    }
                    //End Dynamic Table Creation

                 
                })
                .catch((error) => {
                    this.cdataSource = new kendo.data.DataSource({
                        data: []
                    });
                    this.cdataSource.read();
                    this.setState({ gdata: 'No Records', ChildGridData: [], childVisible: false, data: [], fields: [], title: [] ,showing:false});
                    this.forceUpdate();
                 
                })
        }
        else {
        }
    }
    handleRowTemplate = (e) =>{
        if(!e.index){
            e.index = ++this.count;
        }
        return  e.index;
      }
    
      handleDataBound = (e) =>{
        $('.details').each(function(event){
          $(this).click(()=>{
            let row = $(this).closest('tr')
            console.log(e.sender.dataItem(row))
          })
        })
      }

    render() {
        return (
            <div>
                <Segment>
                    <Header as='h3' >NEO Report</Header>
                </Segment>
                <Segment>
                    <div className={"fltr-div-srch"}>
                        <Input onChange={this.handleKeywordsChange} name={"txtsearch"} icon={'search'} placeholder='Search...' />
                    </div>
                    {this.state.showing_1
                                    ? <div class="k-i-loading"></div>
                                    : null
                                }
                    <div>
                        <Grid
                            className={"grd-height"}
                            dataSource={this.dataSource}
                            dataBound={this.handleDataBound}
                            change={this.onloadgrid}
                            pageable={true}
                            selectable={true}
                            scrollable={false}>
                             <GridColumn field="RowNumber" title="Report No" width="68px" template={this.handleRowTemplate}/>
                            <GridColumn field="caption" title="Report Name" width="250px" />
                            <GridColumn field="created_by" filterable={false} title="Created By" />
                            <GridColumn field="created_date" filterable={false} title="Created Date" />
                            <GridColumn field="updated_date" filterable={false} title="Last Updated" />
                            <GridColumn field="conversion_status" filterable={false} title="Conversion Status" />
                            <GridColumn field="overall_status" filterable={false} title="Overall Status" />
                            {/* <GridColumn template='<button class="k-button submit details" \>select</button\>'/> */}
                        </Grid>
                    </div>
                    {this.state.showing
                                    ? <div class="k-i-loading"></div>
                                    : null
                                }
                </Segment>

                <Segment>
               
                    <div id="example" >
                        <center>
                            <div className={"error"}>{this.state.gdata}</div>
                        </center>
                    </div>
                    <div>
                        {
                            this.state.childVisible
                                ? <Grid id="childGrid"
                                height={300}
                                pageable={true}

                                resizable={true}
                                    dataSource={this.cdataSource}
                                    scrollable={{endless:true}}>
                                    
                                    
                                    </Grid>
                                : null
                        }
                    </div>
                </Segment>
            </div>
        )
    }

}
export default Reports	