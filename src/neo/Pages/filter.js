import React from 'react';
import {allservers} from '../../services/reportService'
import kendo from '@progress/kendo-ui';
import { Grid, GridColumn } from '@progress/kendo-grid-react-wrapper';
import { Button, Segment, Breadcrumb, Input } from 'semantic-ui-react'

class filter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            servername: '',
            serverid:''
        }
        var crudServiceBaseUrl=`${allservers.servers}`
        this.dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl
                }
            },
            batch: true,
            pageSize: 10,
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        host: { type: "string", validation: { required: true } },
                        port: { type: "string", validation: { required: true } },
                        username: { type: "string", validation: { required: true } },
                        name: { type: "string", validation: { required: true } }
                    }
                }
            }
        });
        this.handleKeywordsChange = this.handleKeywordsChange.bind(this);
        this.pageChange = this.pageChange.bind(this);
        this.pageChange1 = this.pageChange1.bind(this);
        this.onloadgrid = this.onloadgrid.bind(this);        
    }

    pageChange() {
        let path = `/manage`;
        this.props.history.push(path);
    }

    pageChange1() {
        let path = `/node`;
        localStorage.ServerID = this.state.serverid;
        this.props.history.servername = this.state.servername;
        this.props.history.serverid = this.state.serverid;
        this.props.history.push(path);
    }

    handleKeywordsChange(e) {       
        var filter = { logic: 'or', filters: [] };
        if (e.target.value != null) {
            filter.filters.push({ field: "name", operator: 'startswith', value: e.target.value })
        }      
        this.dataSource.filter(filter);
    }

    onloadgrid(e) {
        var currentDataItem = e.sender.dataItem(e.sender.select())
        if (currentDataItem !== null) {
            this.setState({ servername: currentDataItem.name,serverid: currentDataItem.id  })
        }
    }

    render() {
        return (
            <div >               
                <div className="topmenu-body">
                    <Segment>
                        <h3>OBI Servers</h3>
                    </Segment>
                    <Breadcrumb>
                        <Breadcrumb.Section href="/home">Activity</Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section active>OBI Servers</Breadcrumb.Section>
                    </Breadcrumb>
                    <Segment >
                        <div className={"fltr-div-srch"} >
                            <Input onChange={this.handleKeywordsChange} name="txtsearch" type="text" icon='search' placeholder='Search...' />
                            <div  className={"fltr-div-srch-div"} >
                                <Button color='teal' type="button" onClick={this.pageChange} className="btn fltr-btn-flot"  >Manage OBI Server</Button>
                            </div>
                        </div>
                        <div>
                            <Grid dataSource={this.dataSource}
                                editable={"inline"}
                                pageable={true}
                                selectable={true}
                                scrollable={false}
                                persistSelection={true}
                                change={this.onloadgrid}>                               
                                <GridColumn field="name" title="Server Name" />
                                <GridColumn field="host" filterable={true} title="Host Name" />
                                <GridColumn field="port" title="Port" width="120px" />
                                <GridColumn field="username" title="User Name" width="120px" />
                            </Grid>
                        </div>
                        <div  className={"fltr-div-srch-div-btm"} >
                            <Button color='teal' disabled={!this.state.servername} type="button" onClick={this.pageChange1} className="btn btn-rad"  >Continue</Button>
                        </div>
                    </Segment>
                </div>
            </div>
        );
    }
}
export default filter;
