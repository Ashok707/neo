import React from "react";
import { Segment, Button } from "semantic-ui-react";
import { getPhysicalDatadigram } from '../../services/reportService';
import $ from 'jquery';
import kendo from '@progress/kendo-ui';
import '../../styles/logicalcard.css';


class PhysicalView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      server_id: [],
      category: [],
      ser_id: '',
      ser_caption: '',
      ser_path: '',
      serverData: [],
      
    };
    this.pageChange = this.pageChange.bind(this);

    if (props.history.physicalview !== undefined) {
      this.state.server_id = props.history.physicalview[1].id
      this.state.category = props.history.physicalview[1].name

    }
    else {
      // let path = `/logicalview`;
      // this.props.history.push(path);
    }
  }

  componentDidMount() {

    var res = localStorage.getItem('serverData')
    var data = JSON.parse(res)
    this.state.serverData.push(data)
   

    $("#loading").prepend("<div class='k-loading-mask' style='width:100%;height:100%'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'><div class='k-loading-color'></div></div></div>");
    getPhysicalDatadigram(data)
      .then((response) => {
        let diagramdata = response.data;
        let diagramconnection = [];
        var input = diagramdata;
        var x = 20, y = 50, z = 0;
        var currxy = 0;
        for (var i = 0; i < input.length; i++) {
          if (input[i].foriegn_key_relationship) {
            var connectto = input[i].foriegn_key_relationship.length;
            for (var j = 0; j < connectto; j++) {
              var totableid = input[i].foriegn_key_relationship[j].destination.tableId;
              for (var k = 0; k < input.length; k++) {
                if (input[k].id === totableid) {
                  var row = {}
                  row["from"] = input[i].foriegn_key_relationship[j].source.tableId;
                  row["to"] = totableid;
                  row["text"] = input[i].foriegn_key_relationship[j].destination.name;
                  diagramconnection.push(row);
                }
              }


            }
          }
          if (z === 0) {
            diagramdata[i]["x"] = x;
            diagramdata[i]["y"] = y;
          }
          else {
            if (i % 2 === 0) {
              if(i===1)
              {
                x=20
              }
              else{

                x += 170;
              }
              currxy = -450;
              //x = 170;

            }
            else {
              if(i===1)
              {
                x=20
              }
              else{
                x += 170;
              }
               //x = 70;
              currxy = 450;
            }
            x = this.x;
            y += currxy;
            diagramdata[i]["x"] = x;
            diagramdata[i]["y"] = y;
          }
          z++;
        }


        var visualTemplate = (options) => {
          var dataviz = kendo.dataviz;
          var g = new dataviz.diagram.Group();
          var dataItem = options.dataItem;
          var currY = 30
          g.append(new dataviz.diagram.Rectangle({
            width: 150,
            height: 30,
            stroke: {
              width: 1,
              color: "#3399dd"
            },
            fill: {
              color: "#3399dd"
            }
          }));


          g.append(new dataviz.diagram.TextBlock({
            text: dataItem.name.toUpperCase(),
            // text: <i class="fa fa-bars"></i>,
            x: 40,
            y: 10,
            fill: "#fff",
            fontSize: 10,

          }));



          for (var i = 0; i < dataItem.columns.length; i++) {
            currY += 15;
            g.append(new dataviz.diagram.TextBlock({
              text: dataItem.columns[i].name,
              x: 5,
              y: currY,
              fill: "#000",
              fontSize: 10
            }));
            g.append(new dataviz.diagram.TextBlock({
              text: dataItem.columns[i].type,
              x: 100,
              y: currY,
              fill: "#000",
              fontSize: 10
            }));
          }
          g.append(new dataviz.diagram.Rectangle({
            width: 150,
            height: dataItem.columns.length + currY + 15,
            stroke: {
              width: 1,
              color: "#3399dd"
            },
            fill: {
            }
          }));

          $("#loading").find("div.k-loading-mask").remove();
          return g;
        }

        $("#diagram").kendoDiagram({
          dataSource: {
            data: diagramdata,
            schema: {
              model: {
                id: "id"
              }
            }
          },
          connectionsDataSource: {
            data: diagramconnection
          },
          layout: null,
          shapeDefaults: {
            visual: visualTemplate
          },
          editable: {
            tools: false
          },
          connectionDefaults: {
            endCap: {
              type: "ArrowEnd",
              fill: "#228B22"
            },
            stroke: {
              color: "#6A5ACD",
              width: 1
            },
            content: {
              template: "#:dataItem.text#",
              fontSize: 10,
              color: "#FF0000",
              fontWeight: "bold"
            }
          }
        })
      })
  }
  pageChange() {
    let path = `/logicalview`;
    this.props.history.push(path);
  }

  onClick=()=>{
    this.props.history.push('/logicalview')
  }
  render() {

    var res = localStorage.getItem('serverData')
    var data = JSON.parse(res)
    var  cap = data.caption
    console.log(data.caption)
    return (
      <div>
        <Segment>
          <h3>Physical View</h3>
        </Segment>
          <Segment>
            <div id="loading"></div>
            <div>
           <div > <h3>{cap}</h3> <br/></div>
            <Button color='blue' onClick={this.onClick} style={{marginLeft:"850px",fontSize: "16px"}}>Click here for Logical view</Button>
         
            <div id="diagram" style={{ fontSize: "6px", marginTop:"20px", height:"1000px" }} > </div>
            </div>
           
          </Segment>

      </div>
    )
  }
}
export default PhysicalView;
