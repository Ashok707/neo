import React from 'react'
import { Segment, Input } from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css';
import $ from 'jquery';
import kendo from '@progress/kendo-ui';
import '../../styles/logicalcard.css';
import { getlogicalData, getlogicalDatadigram } from '../../services/reportService';
import { TabStrip, TabStripTab } from '@progress/kendo-react-layout';


class Logicalview2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      connections: [],
      childVisible: false,
      data: []
      , masterdata: []
      , filterdata: []
      , ChildGridData: []
      , physicalview: []
      , selected: 0
      , visible: true
      , collapse: true
      
    };
    this.handleKeywordsChange = this.handleKeywordsChange.bind(this);

  }


  componentDidMount() {
    $("#loading").prepend("<div class='k-loading-mask' style='width:100%;height:100%'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'><div class='k-loading-color'></div></div></div>");
    getlogicalData()
      .then((response) => {
        this.setState({ filterdata: response.data, masterdata: response.data });

        $("#loading").find("div.k-loading-mask").remove();
      })

  }


  componentWillMount() {

  }

  handleSelect = (e) => {

    this.setState({ selected: e.selected })
    let diagramName = '';
    if (e.selected === 0)
      diagramName = "logicaldiagram";
    else if (e.selected === 1)
      diagramName = "physicaldiagram";

    const user = {
      path: window.localStorage.getItem('ser_path'),
      // "\"caption\"": window.localStorage.getItem('ser_caption'),
      server_id: window.localStorage.getItem('ser_id')
    }
    var visualTemplate = (options) => {
      var dataviz = kendo.dataviz;
      var g = new dataviz.diagram.Group();
      var dataItem = options.dataItem;
      var currY = 30
      g.append(new dataviz.diagram.Rectangle({
        width: 150,
        height: 30,
        stroke: {
          width: 1,
          color: "#3399dd"
        },
        fill: {
          color: "#3399dd"
        }
      }));

      g.append(new dataviz.diagram.TextBlock({
        text: dataItem.name.toUpperCase(),
        // text: <i class="fa fa-bars"></i>,
        x: 40,
        y: 10,
        fill: "#fff",
        fontSize: 10,

      }));

      for (var i = 0; i < dataItem.columns.length; i++) {
        currY += 15;
        g.append(new dataviz.diagram.TextBlock({
          text: dataItem.columns[i].name,
          x: 5,
          y: currY,
          fill: "#000",
          fontSize: 10
        }));
        g.append(new dataviz.diagram.TextBlock({
          text: dataItem.columns[i].type,
          x: 100,
          y: currY,
          fill: "#000",
          fontSize: 10
        }));
      }
      g.append(new dataviz.diagram.Rectangle({
        width: 150,
        height: dataItem.columns.length + currY + 15,
        stroke: {
          width: 1,
          color: "#3399dd"
        },
        fill: {
        }
      }));
      $("#loading").find("div.k-loading-mask").remove();
      return g;
    }
    $("#loading").prepend("<div class='k-loading-mask' style='width:100%;height:100%'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'><div class='k-loading-color'></div></div></div>");
    getlogicalDatadigram(user, diagramName)
      .then((response) => {
        let diagramdata = response.data;
        let diagramconnection = [];
        var input = diagramdata;
        //var x = 20, y = 50;
        //let currxy = 0;
        var connectto ='';
        let MasterConnection = [];
        for (var i = 0; i < input.length; i++) {
          if (e.selected === 0) {
           
            if (input[i].to) {
              connectto = input[i].to.length;
              for (var lj = 0; lj < connectto; lj++) {
                var row = {}
                row["from"] = input[i].id;
                row["to"] = input[i].to[lj];
                row["text"] = "";
                diagramconnection.push(row);
                MasterConnection.push(row);
              }
            }
          }
          else if (e.selected === 1) {
           
            if (input[i].foriegn_key_relationship) {
               connectto = input[i].foriegn_key_relationship.length;
              for (var pj = 0; pj < connectto; pj++) {
                var totableid = input[i].foriegn_key_relationship[pj].destination.tableId;
                for (var pk = 0; pk < input.length; pk++) {
                  if (input[pk].id === totableid) {
                    var prow = {}
                    prow["from"] = input[i].foriegn_key_relationship[pj].source.tableId;
                    prow["to"] = totableid;
                    prow["text"] = input[i].foriegn_key_relationship[pj].destination.name;
                    // prow["srcColId"] = input[i].foriegn_key_relationship[pj].source.colId;
                    // prow["desColId"] = input[i].foriegn_key_relationship[pj].destination.colId;
                     prow["srcColId"] = 'm917bd107-0d77-1000-9a9f-404f5e2b0000';
                     prow["desColId"] = 'm917bd10d-0d77-1000-9a9f-404f5e2b0000';
                    diagramconnection.push(prow);
                  }
                }


              }
            }
            // else
            // {
            //   var prow = {}
            //   prow["from"] = "";
            //   prow["to"] = "";
            //   prow["text"] = "";
            //   // prow["srcColId"] = input[i].foriegn_key_relationship[pj].source.colId;
            //   // prow["desColId"] = input[i].foriegn_key_relationship[pj].destination.colId;
            //    prow["srcColId"] = '';
            //    prow["desColId"] = '';
            //   diagramconnection.push(prow);
            // }
          }
          // if (z === 0) {
          //   diagramdata[i]["x"] = x;
          //   diagramdata[i]["y"] = y;
          // }
          // else {
          //   if (i % 2 === 0) {
          //     if (i === 1) {
          //       x = 20
          //     }
          //     else {

          //       x += 170;
          //     }
          //     currxy = -450;
          //     //x = 170;

          //   }
          //   else {
          //     if (i === 1) {
          //       x = 20
          //     }
          //     else {
          //       x += 170;
          //     }
          //     //x = 70;
          //     currxy = 450;
          //   }
          //   x = x;
          //   y += currxy;
          //   diagramdata[i]["x"] = x;
          //   diagramdata[i]["y"] = y;
          // }
         // z++;
        }
        //Diagram
        let MasterDiagramData = [];
        let xaxis = 50, yaxis = 50;
        let order = 1;
        let Layer = [];
         
        //End       
        const map = new Map();
        const diagramdataMapping = new Map();
        if (e.selected === 0) {
          
          for (var dc = 0; dc < diagramconnection.length; dc++) {
            if (!map.has(diagramconnection[dc].from)) {
              let inputIdx;
              for (let idx = 0; idx < diagramdata.length; idx++) {
                if (diagramdata[idx].id === diagramconnection[dc].from) {
                  inputIdx = idx;
                }
              }
              if (!diagramdataMapping.has(diagramdata[inputIdx].id)) {
                map.set(diagramdata[inputIdx].id, true);
                diagramdataMapping.set(diagramdata[inputIdx].id, true);
                diagramdata[inputIdx].x = xaxis; diagramdata[inputIdx].y = yaxis;
                MasterDiagramData.push(diagramdata[inputIdx]);
                Layer.push({ layer: order, id: diagramdata[inputIdx].id, toid: "", x: xaxis, y: yaxis });

              }
              if (diagramdata[inputIdx].to.length) {
                let tolength = diagramdata[inputIdx].to.length;
                order++; xaxis += 250;
                for (let toIdx = 0; toIdx < tolength; toIdx++) {
                  let inputtoIdx;
                  for (let idx = 0; idx < diagramdata.length; idx++) {

                    if (diagramdata[idx].id === diagramdata[inputIdx].to[toIdx]) {
                      inputtoIdx = idx;
                    }
                  }

                  if (!diagramdataMapping.has(diagramdata[inputtoIdx].id)) {
                    for (let di = 0; di < Layer.length; di++) {
                      if (diagramdata[inputIdx].id === Layer[di].id) {
                        for (let diq = 0; diq < Layer.length; diq++) {
                          if (Layer[di].layer + 1 === Layer[diq].layer) {
                            xaxis = Layer[diq].x;
                            yaxis = Layer[diq].y;
                          }
                        }
                      }
                    }
                    diagramdataMapping.set(diagramdata[inputtoIdx].id, true);
                    diagramdata[inputtoIdx].x = xaxis; diagramdata[inputtoIdx].y = yaxis;
                    MasterDiagramData.push(diagramdata[inputtoIdx]);
                    yaxis += (MasterDiagramData[MasterDiagramData.length - 1].columns.length) * 15 + 75;
                    Layer.push({ layer: order, id: diagramdata[inputIdx].to[toIdx], toid: diagramdata[inputtoIdx].id, x: xaxis, y: yaxis })
                  }
                }
                yaxis = 50;
              }
            }
          }

          if (diagramdata.length > MasterDiagramData.length) {
            for (var idd = 0; idd < diagramdata.length; idd++) {
              let ntabid;
              if(!MasterDiagramData.length ===0)
              for(var idn =0;idn<MasterDiagramData.length;idn++)
              {
                if(MasterDiagramData[idn].id === diagramdata[idd].id)
                {
                  ntabid=idn;
                }

              }
              if (!ntabid) {
                diagramdata[idd].x = xaxis; diagramdata[idd].y = yaxis;
                xaxis +=250;
                MasterDiagramData.push(diagramdata[idd]);
              }
              else
              {
                diagramdata[ntabid].x = xaxis; diagramdata[ntabid].y = yaxis;
                xaxis +=250;
                MasterDiagramData.push(diagramdata[ntabid]);
              }
            }
          }
         
        }
        else if (e.selected === 1) {
         
          //physical Layer
          for (let dc = 0; dc < diagramconnection.length; dc++) {
            if (!map.has(diagramconnection[dc].from)) {
              let inputIdx;
              for (let idx = 0; idx < diagramdata.length; idx++) {
                if (diagramdata[idx].id === diagramconnection[dc].from) {
                  inputIdx = idx;
                }
              }
              if (!diagramdataMapping.has(diagramdata[inputIdx].id)) {
                map.set(diagramdata[inputIdx].id, true);
                diagramdataMapping.set(diagramdata[inputIdx].id, true);
                diagramdata[inputIdx].x = xaxis; diagramdata[inputIdx].y = yaxis;
                MasterDiagramData.push(diagramdata[inputIdx]);
                Layer.push({ layer: order, id: diagramdata[inputIdx].id, toid: "", x: xaxis, y: yaxis });

              }
              if (diagramdata[inputIdx].foriegn_key_relationship.length) {
                let tolength = diagramdata[inputIdx].foriegn_key_relationship.length;
                order++; xaxis += 250;
                for (var toIdx = 0; toIdx < tolength; toIdx++) {
                  let inputtoIdx;
                  for (var tidx = 0; tidx < diagramdata.length; tidx++) {

                    if (diagramdata[tidx].id === diagramdata[inputIdx].foriegn_key_relationship[toIdx].destination.tableId) {
                      inputtoIdx = tidx;
                    }
                  }
                  if (inputtoIdx || inputtoIdx === 0)
                    if (!diagramdataMapping.has(diagramdata[inputtoIdx].id)) {
                      for (var di = 0; di < Layer.length; di++) {
                        if (diagramdata[inputIdx].id === Layer[di].id) {
                          for (var diq = 0; diq < Layer.length; diq++) {
                            if (Layer[di].layer + 1 === Layer[diq].layer) {
                              xaxis = Layer[diq].x;
                              yaxis = Layer[diq].y;
                            }
                          }
                        }
                      }
                      
                      diagramdataMapping.set(diagramdata[inputtoIdx].id, true);
                      diagramdata[inputtoIdx].x = xaxis; diagramdata[inputtoIdx].y = yaxis;
                      MasterDiagramData.push(diagramdata[inputtoIdx]);
                      yaxis += (MasterDiagramData[MasterDiagramData.length - 1].columns.length) * 15 + 75;
                      Layer.push({ layer: order, id: diagramdata[inputIdx].foriegn_key_relationship[toIdx].destination.tableId, toid: diagramdata[inputtoIdx].id, x: xaxis, y: yaxis })
                    }
                }
                yaxis = 50;
              }
            }
          }
             
          for (var conId = 0; conId < diagramconnection.length; conId++) {
            var Connectionrow = {}
            let inputIdx,inputtoIdx, srcColIdx,desColIdx;
            let FromX, FromY, ToX, ToY;
            for (var sidx = 0; sidx < MasterDiagramData.length; sidx++) {
              if (MasterDiagramData[sidx].id === diagramconnection[conId].from) {
                inputIdx = sidx;
              }
            }
            for (var col = 0; col < MasterDiagramData[inputIdx].columns.length; col++) {
              if (MasterDiagramData[inputIdx].columns[col].id === diagramconnection[conId].srcColId) {
                srcColIdx = col + 1;                   
                FromX=MasterDiagramData[inputIdx].x+150;
                FromY=MasterDiagramData[inputIdx].y+50+(srcColIdx*10);                      
                break;
              }
            }
            for (var didx = 0; didx < MasterDiagramData.length; didx++) {
              if (MasterDiagramData[didx].id === diagramconnection[conId].to) {
                inputtoIdx = didx;
              }
            }
            for (var dcol = 0; dcol < MasterDiagramData[inputtoIdx].columns.length; dcol++) {
              if (MasterDiagramData[inputtoIdx].columns[dcol].id === diagramconnection[conId].desColId) {
                desColIdx = dcol + 1;                   
                ToX=MasterDiagramData[inputtoIdx].x;
                ToY=MasterDiagramData[inputtoIdx].y+50+(desColIdx*10) //100+(desColIdx*10);
                break;
              }
            }                
            Connectionrow["fromX"]=FromX
            Connectionrow["fromY"]=FromY
            Connectionrow["toX"]=ToX
            Connectionrow["toY"]=ToY
            Connectionrow["text"]=diagramconnection[conId].text;
            Connectionrow["type"]= "polyline";
            if(FromX)if(FromY)if(ToX)if(ToY)
            MasterConnection.push(Connectionrow);   
               
          }

          if (diagramdata.length > MasterDiagramData.length) {
            for ( idd = 0; idd < diagramdata.length; idd++) {
              let ntabid;
              if(!MasterDiagramData.length ===0)
              for( idn =0;idn<MasterDiagramData.length;idn++)
              {
                if(MasterDiagramData[idn].id === diagramdata[idd].id)
                {
                  ntabid=idn;
                }

              }
              if (!ntabid) {
                diagramdata[idd].x = xaxis; diagramdata[idd].y = yaxis;
                xaxis +=250;
                MasterDiagramData.push(diagramdata[idd]);
              }
              else
              {
                diagramdata[ntabid].x = xaxis; diagramdata[ntabid].y = yaxis;
                xaxis +=250;
                MasterDiagramData.push(diagramdata[ntabid]);
              }
            }
          }

        }

        //$("#loading").find("div.k-loading-mask").remove();
        $("#" + diagramName + window.localStorage.getItem('ser_CardId')).kendoDiagram({
          dataSource: {
            data: MasterDiagramData,
            schema: {
              model: {
                id: "id"
              }
            }
          },
          connectionsDataSource: {
            data: MasterConnection
          },
          layout: null,
          shapeDefaults: {
            visual: visualTemplate
          },
          selectable: false,
          editable: {
            tools: false
          },
          connectionDefaults: {
            startCap: {
              type: "FilledCircle",
              fill: "#228B22"
            },
            endCap: {
              type: "ArrowEnd",
              fill: "#228B22"
            },
            stroke: {
              color: "#2713DF",
              width: 2
            },
            content: {
              template: "#:dataItem.text#",
              fontSize: 7,
              color: "#FF0000"
            }
          },

        });
      })


  }

  componentDidUpdate() {
    //this.state.selected = 0;
    
    //this.setState({ selected: 0 });
    if (this.state.visible === true && this.state.collapse === true) {
      var visualTemplate = (options) => {
        var dataviz = kendo.dataviz;
        var g = new dataviz.diagram.Group();
        var dataItem = options.dataItem;
        var currY = 30
        g.append(new dataviz.diagram.Rectangle({
          width: 150,
          height: 30,
          stroke: {
            width: 1,
            color: "#3399dd"
          },
          fill: {
            color: "#3399dd"
          }
        }));

        g.append(new dataviz.diagram.TextBlock({
          text: dataItem.name.toUpperCase(),
          // text: <i class="fa fa-bars"></i>,
          x: 40,
          y: 10,
          fill: "#fff",
          fontSize: 10,
        }));

        for (var i = 0; i < dataItem.columns.length; i++) {
          currY += 15;
          g.append(new dataviz.diagram.TextBlock({
            text: dataItem.columns[i].name,
            x: 5,
            y: currY,
            fill: "#000",
            fontSize: 10
          }));

          g.append(new dataviz.diagram.TextBlock({
            text: dataItem.columns[i].type,
            x: 100,
            y: currY,
            fill: "#000",
            fontSize: 10
          }));
        }
        g.append(new dataviz.diagram.Rectangle({
          width: 150,
          height: dataItem.columns.length + currY + 15,
          stroke: {
            width: 1,
            color: "#3399dd"
          },
          fill: {
          }
        }));
        $("#loading").find("div.k-loading-mask").remove();
        return g;
      }
      var $cell = $('.card');
      //open and close card when clicked on card
      $cell.find('.js-expander').click(function () {
        var $thisCell = $(this).closest('.card');
        if ($thisCell.hasClass('is-collapsed')) {
          $cell.not($thisCell).removeClass('is-expanded').addClass('is-collapsed').addClass('is-inactive');
          $thisCell.removeClass('is-collapsed').addClass('is-expanded');
          //if ($cell.not($thisCell).hasClass('is-inactive')) {
            var id = $thisCell[0].id;
            var value = $("#" + id + " :input");
            const user = {
              path: value[0].value,
              // "\"caption\"": value[0].name,
              server_id: value[0].id
            }
            window.localStorage.setItem("ser_id", user.server_id)
            window.localStorage.setItem("ser_CardId", $thisCell[0].id)
            // window.localStorage.setItem("ser_caption", user.caption)
            window.localStorage.setItem("ser_path", user.path)
            $("#loading").prepend("<div class='k-loading-mask' style='width:100%;height:100%'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'><div class='k-loading-color'></div></div></div>");
            getlogicalDatadigram(user, "logicaldiagram")
              .then((response) => {
                let diagramdata = response.data;
                let diagramconnection = [];
                var input = diagramdata;
                var x = 20, y = 50, z = 0;
                var currxy = 0;
                // this.handleSelect(this);
                for (var i = 0; i < input.length; i++) {
                  if (input[i].to) {
                    var connectto = input[i].to.length;
                    for (var j = 0; j < connectto; j++) {
                      var row = {}
                      row["from"] = input[i].id;
                      row["to"] = input[i].to[j];
                      row["text"] = "";
                      diagramconnection.push(row);
                    }
                  }
                  // else
                  // {
                  //   var row = {}
                  //   row["from"] = "";
                  //   row["to"] = "";
                  //   row["text"] = "";
                  //   //diagramconnection.push(row);
                  // }
                  if (z === 0) {
                    diagramdata[i]["x"] = x;
                    diagramdata[i]["y"] = y;
                  }
                  else {
                    if (i % 2 === 0) {
                      if (i === 1) {
                        x = 20
                      }
                      else {

                        x += 170;
                      }
                      currxy = -450;
                      //x = 170;

                    }
                    else {
                      if (i === 1) {
                        x = 20
                      }
                      else {
                        x += 170;
                      }
                      //x = 70;
                      currxy = 450;
                    }
                    //x = x;
                    y += currxy;
                    diagramdata[i]["x"] = x;
                    diagramdata[i]["y"] = y;
                  }
                  z++;
                }

                let MasterDiagramData = [];
                let xaxis = 50, yaxis = 50;
                let order = 1;
                let Layer = [];
                const map = new Map();
                const diagramdataMapping = new Map();
               
                for (var dc = 0; dc < diagramconnection.length; dc++) {
                  if (!map.has(diagramconnection[dc].from)) {
                    let inputIdx;
                    for (var idx = 0; idx < diagramdata.length; idx++) {
                      if (diagramdata[idx].id === diagramconnection[dc].from) {
                        inputIdx = idx;
                      }
                    }
                    if (!diagramdataMapping.has(diagramdata[inputIdx].id)) {
                      map.set(diagramdata[inputIdx].id, true);
                      diagramdataMapping.set(diagramdata[inputIdx].id, true);
                      diagramdata[inputIdx].x = xaxis; diagramdata[inputIdx].y = yaxis;
                      MasterDiagramData.push(diagramdata[inputIdx]);
                      Layer.push({ layer: order, id: diagramdata[inputIdx].id, toid: "", x: xaxis, y: yaxis });

                    }
                    if (diagramdata[inputIdx].to.length) {
                      let tolength = diagramdata[inputIdx].to.length;
                      order++; xaxis += 250;
                      for (let toIdx = 0; toIdx < tolength; toIdx++) {
                        let inputtoIdx;
                        for (let idx = 0; idx < diagramdata.length; idx++) {

                          if (diagramdata[idx].id === diagramdata[inputIdx].to[toIdx]) {
                            inputtoIdx = idx;
                          }
                        }

                        if (!diagramdataMapping.has(diagramdata[inputtoIdx].id)) {
                          for (let di = 0; di < Layer.length; di++) {
                            if (diagramdata[inputIdx].id === Layer[di].id) {
                              for (let diq = 0; diq < Layer.length; diq++) {
                                if (Layer[di].layer + 1 === Layer[diq].layer) {
                                  xaxis = Layer[diq].x;
                                  yaxis = Layer[diq].y;
                                }
                              }
                            }
                          }
                          diagramdataMapping.set(diagramdata[inputtoIdx].id, true);
                          diagramdata[inputtoIdx].x = xaxis; diagramdata[inputtoIdx].y = yaxis;
                          MasterDiagramData.push(diagramdata[inputtoIdx]);
                          yaxis += (MasterDiagramData[MasterDiagramData.length - 1].columns.length) * 15 + 75;
                          Layer.push({ layer: order, id: diagramdata[inputIdx].to[toIdx], toid: diagramdata[inputtoIdx].id, x: xaxis, y: yaxis })
                        }
                      }
                      yaxis = 50;
                    }

                  }
                }

                if (diagramdata.length > MasterDiagramData.length) {
                  for (var idd = 0; idd < diagramdata.length; idd++) {
                    let ntabid;
                    if(!MasterDiagramData.length ===0)
                    for(var idn =0;idn<MasterDiagramData.length;idn++)
                    {
                      if(MasterDiagramData[idn].id === diagramdata[idd].id)
                      {
                        ntabid=idn;
                      }
      
                    }
                    if (!ntabid) {
                      diagramdata[idd].x = xaxis; diagramdata[idd].y = yaxis;
                      xaxis +=250;
                      MasterDiagramData.push(diagramdata[idd]);
                    }
                    else
                    {
                      diagramdata[ntabid].x = xaxis; diagramdata[ntabid].y = yaxis;
                      xaxis +=250;
                      MasterDiagramData.push(diagramdata[ntabid]);
                    }
                  }
                }

                $("#loading").find("div.k-loading-mask").remove();
                $("#logicaldiagram" + $thisCell[0].id).kendoDiagram({
                  dataSource: {
                    data: MasterDiagramData,
                    schema: {
                      model: {
                        id: "id"
                      }
                    }
                  },
                  connectionsDataSource: {
                    data: diagramconnection
                  },
                  layout: null,
                  shapeDefaults: {
                    visual: visualTemplate
                  },
                  selectable: false,
                  editable: {
                    tools: false
                  },
                  connectionDefaults: {
                    startCap: {
                      type: "FilledCircle",
                      fill: "#228B22"
                    },
                    endCap: {
                      type: "ArrowEnd",
                      fill: "#228B22"
                    },
                    stroke: {
                      color: "#2713DF",
                      width: 2
                    },
                    content: {
                      template: "#:dataItem.text#",
                      fontSize: 7,
                      color: "#FF0000"
                    }
                  },

                });
              })
              .catch((error) => {
                $("#loading").find("div.k-loading-mask").remove();
              });
          // } 
          // else {
          //   $cell.not($thisCell).addClass('is-inactive');
          // }
        } 
        else {
          $thisCell.removeClass('is-expanded').addClass('is-collapsed');
          $cell.not($thisCell).removeClass('is-inactive');
        }
      });

      //close card when click on cross
      $cell.find('.js-collapser').click(function () {
        var $thisCell = $(this).closest('.card');
        $thisCell.removeClass('is-expanded').addClass('is-collapsed');
        $cell.not($thisCell).removeClass('is-inactive');

      });
    }
  }

  handleKeywordsChange(e) {
    var $cell = $('.card');
    if (e.target.value != null) {

      var $thisCell = $(this).closest('.card');
      $cell.not($thisCell).removeClass('is-inactive');
      const tablenames = this.state.masterdata;
      const startsWithN = tablenames.filter((table) => table.caption.startsWith(e.target.value));
      this.setState({ filterdata: startsWithN });
    }

    $cell.find('.js-collapser').click(function () {
      var $thisCell = $(this).closest('.card');
      $thisCell.removeClass('is-expanded').addClass('is-collapsed');
      $cell.not.removeClass('is-inactive');


    });
    //this.setState({ filterdata: this.state.masterdata });   

  }
  btnclick = () => {
    this.setState({
      visible: false,
      collapse: true,
      selected:0
    })
  }
  render() {
    return (
      <div>
        <Segment>
          <h3>Logical View</h3>

        </Segment>
        <Segment>

          <div id="loading">
          </div>
          <div className={"fltr-div-srch"} >
            <Input
              onChange={this.handleKeywordsChange}
              name="txtsearch" type="text" icon='search' placeholder='Search...' />
          </div>
          <div class='wrapper'><div class='cards'>


            {
              this.state.filterdata.map((user, i) =>
                <div id={i} title={user.caption} class=' card [ is-collapsed ] '>
                  <div class='card__inner [ js-expander ]' onClick={this.btnclick}>
                    <input type="hidden" id={user.server_id} name={user.caption} value={user.path}></input>
                    {/* <input type="hidden" id={user.path}></input> */}
                    <span>{user.caption}</span>
                    <i class='fa fa-folder-o'></i>
                  </div>
                  <div class='card__expander'>
                    <TabStrip selected={this.state.selected} onSelect={this.handleSelect}>
                      <TabStripTab title="Logical View">
                        <div id={"logicaldiagram" + i} style={{ fontSize: "6px" }} ></div>

                      </TabStripTab>
                      <TabStripTab title="Physical View">
                        <div id={"physicaldiagram" + i} style={{ fontSize: "6px" }} ></div>
                      </TabStripTab>

                    </TabStrip>
                    {/* <TabStrip   >

                      <TabStripTab title="Logical View" >
                       <div style={{ width: '1000px' }}>
                       <div id={"diagram" + i} style={{ fontSize: "6px"  }} ></div>
                        </div>                     
                      </TabStripTab>

                      <TabStripTab title="Physical View" >
                      <button class='zoom-in k-button'>Logical View</button>                   
                      </TabStripTab>

                    </TabStrip> */}


                    <i style={{ color: 'red' }} class='fa fa-close [ js-collapser ]'></i>


                  </div>
                </div>)
            }
          </div>
          </div>

        </Segment>
      </div>
    );
  }
}
export default Logicalview2;