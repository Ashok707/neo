import React from 'react';
import Toplayout from '../layout/toplayout';
import kendo from '@progress/kendo-ui';
import { Grid, GridColumn } from '@progress/kendo-grid-react-wrapper';
import { Button, Header,Segment, Breadcrumb, Input } from 'semantic-ui-react';
import {allservers} from '../../services/reportService';

class manage extends React.Component {
    constructor(props) {
        super(props);
        var crudServiceBaseUrl = `${allservers.servers}`
        this.dataSource = new kendo.data.DataSource({

            transport: {
                read: {
                    url: crudServiceBaseUrl
                },
                update: {

                    url: function (options, operation) {

                        return crudServiceBaseUrl + options.models[0].id


                    },

                    type: "PUT",

                    contentType: "application/json"

                },
                destroy: {
                    url: function (options, operation) {

                        return crudServiceBaseUrl + options.models[0].id

                    },

                    type: "DELETE"
                },

                parameterMap: function (options, operation) {
                
                    if (operation === "update" && options.models) {

                        return kendo.stringify(options.models[0])

                    }


                }
            },


            batch: true,
            pageSize: 10,
            schema: {
                model: {
                    id: "id",
                    fields: {

                        id: { editable: false, nullable: true },
                        host: { type: "string", validation: { required: true } },
                        port: { type: "string", validation: { required: true } },
                        username: { type: "string", validation: { required: true } },
                        name: { type: "string", validation: { required: true } }
                    }

                }
            }

        });
        this.handleKeywordsChange = this.handleKeywordsChange.bind(this);
        this.pageChange = this.pageChange.bind(this);

        this.pageChangeadd = this.pageChangeadd.bind(this);
        
    }


    pageChange() {
        let path = `/neoreports`;
        this.props.history.push(path);
    }

    pageChangeadd() {
        let path = `/Datasource`;
        this.props.history.push(path);
    }



    handleKeywordsChange(e) {     

       
        var filter = { logic: 'or', filters: [] };
        if (e.target.value != null) {
            filter.filters.push({ field: "name", operator: 'startswith', value: e.target.value })
        }
        this.dataSource.filter(filter);

    }

    render() {
        return (



            <div >
                <Toplayout></Toplayout>
                <div className="topmenu-body">
                    <Segment>
                        <Header as='h3'>Manage Servers</Header>
                    </Segment>
                    <Breadcrumb>
                        <Breadcrumb.Section href="/home">Activity</Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section href="/filter">OBI Servers</Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section active >Manage Servers</Breadcrumb.Section>
                    </Breadcrumb>

                    <Segment >
                        <div className={"fltr-div-srch"} >
                        <Input onChange={this.handleKeywordsChange} name="txtsearch" type="text" icon='search' placeholder='Search...' />
                            <div className={"fltr-div-srch-div"}>
                                <Button color='teal' type="button" onClick={this.pageChangeadd} className="btn fltr-btn-flot"  >Add Datasource</Button>
                            </div>
                        </div>
                        <div>
                            <Grid dataSource={this.dataSource}
                                editable={"inline"}
                                pageable={true}
                                selectable={true}
                                scrollable={false}
                                persistSelection={true}
                            >
                                <GridColumn title="<input id='chkAll' class='checkAllCls' type='checkbox'/>" width="35px" template="<input type='checkbox' class='check-box-inner' />" filterable="false" />
                                <GridColumn field="name" title="Server Name" />
                                <GridColumn field="host" filterable={true} title="Host Name" />
                                <GridColumn field="port" title="Port" width="120px" />
                                <GridColumn field="username" title="User Name" width="120px" />

                                <GridColumn command={["edit", "destroy"]} title="&nbsp;" width="100px" />
                            </Grid>
                        </div>
                        <div  className={"fltr-div-srch-div-btm"} >
                            <Button color='teal' type="button" onClick={this.pageChange} className="btn btn-rad" >Continue</Button>
                        </div>
                    </Segment>
                </div>
            </div>
        );
    }
}


export default manage;
