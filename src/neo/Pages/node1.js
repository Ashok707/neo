import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { TreeView } from '@progress/kendo-react-treeview';
import { Grid, GridColumn as Column } from '@progress/kendo-react-grid';
import product from '../Json/datatwo.json';
import { Segment, Button } from "semantic-ui-react";

import { DropDownList } from '@progress/kendo-react-dropdowns';
import axios from 'axios'
import TopMenu from '../layout/TopMenu';

const baseUrl = 'http://64.79.74.156:3001/servers/connect/' + 'c7a8d839-90ed-418e-b4ba-2ac7510edb4c';
const baseUrlPOST = 'http://64.79.74.156:3001/servers/' + 'c7a8d839-90ed-418e-b4ba-2ac7510edb4c';
const requestInit = { method: 'GET', accept: 'application/json', headers: [] };

class node extends Component {

    constructor(props)
    {

        super(props);
       
        this.state = {

            
            data: []
          };
    }
render()
{

    return(

        <div>
      
        <TreeView
                  data={this.state.data}
                 onExpandChange ={this.onExpandChange}
                />
        </div>
    )
}


   
// Page load to load the parent folder 
componentDidMount() {
    fetch(baseUrl, requestInit)
      .then(response => response.json())
      .then(json => {
        const data = json.map(({ path, caption, type }) => {
          return { text: caption, id: path, type: type, hasChildren: true };
        });

        this.setState({ data });
        console.log(this.state.data);
      });
  }

//To load subfolders

  onExpandChange = event => {
    const categoryIndex = Number(event.itemHierarchicalIndex);
    const categoryIndex1 = event.itemHierarchicalIndex;
    var len = categoryIndex1.length;

    console.log(this.state.data);
    console.log(event.item.type);

    var pathofreq = event.item.id;
    console.log(pathofreq);

    if (event.item.type == "Object") {
      

      const requestInitPOST = {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/json" // <-- Specifying the Content-Type
        }),
        body: JSON.stringify({
          path: pathofreq
        })
      };
      fetch(
        `http://64.79.74.156:3001/servers/c7a8d839-90ed-418e-b4ba-2ac7510edb4c/reportdetails`,
        requestInitPOST
      )
        .then(response => response.json())
        .then(response => {
          if (response == "") {
            alert("No data Found");
          } else {
            var newItemInput = response.column_count;
            const obj = {
              count: newItemInput,
              path: pathofreq,
              name: event.item.text,
              hasChildren: false,
              expanded: false
            };
            const newArray = this.state.selItemArray.slice(); // Create a copy
            newArray.push(obj); // Push the object
            this.setState({ selItemArray: newArray });
            console.log(this.state.selItemArray);
          }
        });
    } else {
      const { data, category } = this.copyDataAndCategory(categoryIndex1);
      category.expanded = !category.expanded;

      this.setState({ data });

      !category.items && this.loadNodes(categoryIndex1, event.item.id);
    }
  };

  // To slice the underscore values

  copyDataAndCategory(categoryIndex) {
    var _count = categoryIndex.toString().split("_");

    if (categoryIndex.toString().indexOf("_") == -1) {
      var data = this.state.data.slice();
      var category = (data[categoryIndex] = { ...data[categoryIndex] });

      console.log(data, category, "category");
      return { data, category };
    } else if (_count.length == 2) {
      var data = this.state.data.slice();
      var category = (data[Number(categoryIndex.substring(0, 1))].items[
        Number(categoryIndex.substring(2, 4))
      ] = {
        ...data[Number(categoryIndex.substring(0, 1))].items[
          Number(categoryIndex.substring(2, 4))
        ]
      });
      console.log(data, category, "category");
      return { data, category };
    } else if (_count.length == 3) {
      var data = this.state.data.slice();

      var category = (data[Number(categoryIndex.substring(0, 1))].items[
        Number(categoryIndex.substring(2, 3))
      ].items[Number(categoryIndex.substring(4, 5))] = {
        ...data[Number(categoryIndex.substring(0, 1))].items[
          Number(categoryIndex.substring(2, 3))
        ].items[Number(categoryIndex.substring(4, 5))]
      });
      console.log(data, category, "category");
      return { data, category };
    } else if (_count.length == 4) {
      var data = this.state.data.slice();

      var category = (data[Number(categoryIndex.substring(0, 1))].items[
        Number(categoryIndex.substring(2, 3))
      ].items[Number(categoryIndex.substring(4, 5))].items[
        Number(categoryIndex.substring(6, 7))
      ] = {
        ...data[Number(categoryIndex.substring(0, 1))].items[
          Number(categoryIndex.substring(2, 3))
        ].items[Number(categoryIndex.substring(4, 5))].items[
          Number(categoryIndex.substring(6, 7))
        ]
      });
      console.log(data, category, "category");
      return { data, category };
    } else if (_count.length == 5) {
      var data = this.state.data.slice();

      var category = (data[Number(categoryIndex.substring(0, 1))].items[
        Number(categoryIndex.substring(2, 3))
      ].items[Number(categoryIndex.substring(4, 5))].items[
        Number(categoryIndex.substring(6, 7))
      ].items[Number(categoryIndex.substring(8, 9))] = {
        ...data[Number(categoryIndex.substring(0, 1))].items[
          Number(categoryIndex.substring(2, 3))
        ].items[Number(categoryIndex.substring(4, 5))].items[
          Number(categoryIndex.substring(6, 7))
        ].items[Number(categoryIndex.substring(8, 9))]
      });
      console.log(data, category, "category");
      return { data, category };
    }
  }

  // to load  sub nodes
  loadNodes(categoryIndex, categoryId) {
    const requestInitPOST = {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json" // <-- Specifying the Content-Type
      }),
      body: JSON.stringify({
        path: categoryId
      })
    };
    fetch(`${baseUrlPOST}/subitems`, requestInitPOST)
      .then(response => response.json())
      .then(json => {
        if (json == "") {
          alert("No Data Found");
        } else {
          const { data, category } = this.copyDataAndCategory(categoryIndex);

          category.items = json.map(({ caption, path, type }) => {
            return { text: caption, id: path, type: type, hasChildren: true };
          });

          this.setState({ data });

          !category.items && this.loadNodes(categoryIndex, category.id);
        }
      });
  }

}  

export default node;
