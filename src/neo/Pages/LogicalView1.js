import React from 'react'
import { Segment, Input } from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css';
import $ from 'jquery';
import kendo from '@progress/kendo-ui';
import '../../styles/logicalcard.css';
import { getlogicalData, getlogicalDatadigram, getPhysicalDatadigram } from '../../services/reportService';
import { Collapse, Button, CardBody, Card } from 'reactstrap';

class Logicalview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      connections: [],
      childVisible: false,
      data: []
      , lrows: []
      , ChildGridData: []
      , physicalview: []
      , selected: 0
      , button: true
      , button1: true
      , respData: localStorage.getItem('physical')
      , collapse: true
      , coll: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    if (this.state.collapse === false)
      this.setState({ collapse: true, coll: false });
    else {
      this.setState({ collapse: false, coll: true });
    }

  }

  componentDidMount() {
    $("#loading").prepend("<div class='k-loading-mask' style='width:100%;height:100%'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'><div class='k-loading-color'></div></div></div>");
    getlogicalData()
      .then((response) => {
        this.setState({ lrows: response.data });

        $("#loading").find("div.k-loading-mask").remove();
      })
  }





  handleSelect = () => {
    var onclick = () => {
      alert("ji")
    }

    var visualTemplate = (options) => {
      var dataviz = kendo.dataviz;
      var g = new dataviz.diagram.Group();
      var dataItem = options.dataItem;
      var currY = 30
      g.append(new dataviz.diagram.Rectangle({
        width: 150,
        height: 30,
        stroke: {
          width: 1,
          color: "#3399dd"
        },
        fill: {
          color: "#3399dd"
        }
      }));


      g.append(new dataviz.diagram.TextBlock({
        text: dataItem.name.toUpperCase(),
        // text: <i class="fa fa-bars"></i>,
        x: 40,
        y: 10,
        fill: "#fff",
        fontSize: 10,

      }));



      for (var i = 0; i < dataItem.columns.length; i++) {
        currY += 15;
        g.append(new dataviz.diagram.TextBlock({
          text: dataItem.columns[i].name,
          x: 5,
          y: currY,
          fill: "#000",
          fontSize: 10
        }));
        g.append(new dataviz.diagram.TextBlock({
          text: dataItem.columns[i].type,
          x: 100,
          y: currY,
          fill: "#000",
          fontSize: 10
        }));
      }
      g.append(new dataviz.diagram.Rectangle({
        width: 150,
        height: dataItem.columns.length + currY + 15,
        stroke: {
          width: 1,
          color: "#3399dd"
        },
        fill: {
        }
      }));

      $("#loading").find("div.k-loading-mask").remove();
      return g;
    }

    var $cell = $('.card');
    // console.log($cell)

    //open and close card when clicked on card
    $cell.find('.js-expander').click(function () {

      var $thisCell = $(this).closest('.card');
      console.log($thisCell)
      if ($thisCell.hasClass('is-collapsed')) {
        $cell.not($thisCell).removeClass('is-expanded').addClass('is-collapsed').addClass('is-inactive');
        $thisCell.removeClass('is-collapsed').addClass('is-expanded');
        if ($cell.not($thisCell).hasClass('is-inactive')) {
          //do nothing
          //  console.log($thisCell);
          const user = {
            path: $thisCell[0].slot,
            caption: $thisCell[0].title,
            server_id: $thisCell[0].nonce
          }
          localStorage.setItem('serverData', JSON.stringify(user))
          $("#loading").prepend("<div class='k-loading-mask' style='width:100%;height:100%'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'><div class='k-loading-color'></div></div></div>");

          getlogicalDatadigram(user)
            .then((response) => {
              let diagramdata = response.data;
              let diagramconnection = [];
              var input = diagramdata;
              var x = 20, y = 50, z = 0;
              var currxy = 0;
              for (var i = 0; i < input.length; i++) {
                if (input[i].to) {
                  var connectto = input[i].to.length;
                  for (var j = 0; j < connectto; j++) {
                    var row = {}
                    row["from"] = input[i].id;
                    row["to"] = input[i].to[j];
                    row["text"] = input[i].id;
                    diagramconnection.push(row);
                  }
                }
                if (z === 0) {
                  diagramdata[i]["x"] = x;
                  diagramdata[i]["y"] = y;
                }
                else {
                  if (i % 2 === 0) {
                    currxy = -250;
                  }
                  else {
                    currxy = 250;
                  }
                  x += 170;
                  y += currxy;
                  diagramdata[i]["x"] = x;
                  diagramdata[i]["y"] = y;
                }
                z++;
              }

              $("#loading").find("div.k-loading-mask").remove();
              $("#diagram" + $thisCell[0].id).kendoDiagram({
                dataSource: {
                  data: diagramdata,
                  schema: {
                    model: {
                      id: "id"
                    }
                  }
                },
                connectionsDataSource: {
                  data: diagramconnection
                },
                layout: null,
                shapeDefaults: {
                  visual: visualTemplate
                },
                editable: {
                  tools: false
                },
                connectionDefaults: {
                  endCap: {
                    type: "ArrowEnd",
                    fill: "#228B22"
                  },
                  stroke: {
                    color: "#6A5ACD",
                    width: 1
                  },
                  content: {
                    template: "#:dataItem.text#",
                    fontSize: 7,
                    color: "#FF0000"
                  }
                },
                click: onclick
              });
              $("#loading").prepend("<div class='k-loading-mask' style='width:100%;height:100%'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'><div class='k-loading-color'></div></div></div>");
              getPhysicalDatadigram(user)
                .then((response) => {
                  let diagramdata = response.data;
                  let diagramconnection = [];
                  var input = diagramdata;
                  var x = 20, y = 50, z = 0;
                  var currxy = 0;
                  for (var i = 0; i < input.length; i++) {

                    if (input[i].foriegn_key_relationship) {
                      var connectto = input[i].foriegn_key_relationship.length;
                      for (var j = 0; j < connectto; j++) {
                        var row = {}
                        row["from"] = input[i].foriegn_key_relationship[j].source.tableId;
                        row["to"] = input[i].foriegn_key_relationship[j].destination.tableId;
                        row["text"] = input[i].foriegn_key_relationship[j].destination.tableName;
                        diagramconnection.push(row);
                      }
                    }
                    if (z === 0) {
                      diagramdata[i]["x"] = x;
                      diagramdata[i]["y"] = y;
                    }
                    else {
                      if (i % 2 === 0) {
                        currxy = -250;
                      }
                      else {
                        currxy = 250;
                      }
                      x += 170;
                      y += currxy;
                      diagramdata[i]["x"] = x;
                      diagramdata[i]["y"] = y;
                    }
                    z++;
                  }
                  $("#loading").find("div.k-loading-mask").remove();
                  $("#diagramTwo" + $thisCell[0].id).kendoDiagram({
                    dataSource: {
                      data: diagramdata,
                      schema: {
                        model: {
                          id: "id"
                        }
                      }
                    },
                    connectionsDataSource: {
                      data: diagramconnection
                    },
                    layout: null,
                    shapeDefaults: {
                      visual: visualTemplate
                    },
                    editable: {
                      tools: false
                    },
                    connectionDefaults: {
                      endCap: {
                        type: "ArrowEnd",
                        fill: "#228B22"
                      },
                      stroke: {
                        color: "#6A5ACD",
                        width: 1
                      },
                      content: {
                        template: "#:dataItem.text#",
                        fontSize: 7,
                        color: "#FF0000"
                      }
                    }
                  });

                })

            })
            .catch((error) => {
              $("#loading").find("div.k-loading-mask").remove();
            });

        } else {
          $cell.not($thisCell).addClass('is-inactive');
        }

      } else {
        $thisCell.removeClass('is-expanded').addClass('is-collapsed');
        $cell.not($thisCell).removeClass('is-inactive');
      }
    });

    //close card when click on cross
    $cell.find('.js-collapser').click(function () {
      var $thisCell = $(this).closest('.card');
      console.log($thisCell)
      $thisCell.removeClass('is-expanded').addClass('is-collapsed');
      $cell.not($thisCell).removeClass('is-inactive');

    });


  }

  componentDidUpdate = (e) => {
    if (this.state.button === true) {

      this.handleSelect()
    }
   

  }

  btnclick = () => {
    console.log("btn click")
      
    this.setState({
      button: false,
      collapse:true,
      coll:false
    })
   

    // this.btnclick1()
  }

  onClick = () => {

    this.props.history.push('/physicalview')


  }

  render() {
    if (this.state.collapse === true) {

    }
    return (
      <div>
        <Segment>
          <h3>Logical View</h3>

        </Segment>
        <Segment>

          <div id="loading">
          </div>
          <div className={"fltr-div-srch"} >
            <Input
              // onChange={this.handleKeywordsChange} 
              name="txtsearch" type="text" icon='search' placeholder='Search...' />
          </div>
          <div class='wrapper'><div class='cards'>
            {

              this.state.lrows.map((user, i) =>
                <div id={i} title={user.caption} nonce={user.server_id} slot={user.path} class=' card [ is-collapsed ] ' >
                  <div class='card__inner [ js-expander ]' onClick={this.btnclick}>
                    <span>{user.caption}</span>
                    <i class='fa fa-folder-o'></i>
                  </div>

                  <div class={this.state.collapse && 'card__expander'}>

                    <Collapse isOpen={this.state.collapse}>
                      <Card>
                        <CardBody>
                          <Button color="primary" onClick={this.toggle} style={{ marginBottom: '1rem', marginLeft: "700px" }}>Click here to see Physical View</Button>
                          <div style={{ width: '1100px', marginTop: "20px", marginLeft:"-50px" }}>

                            <span style={{ color: "red" }}>Logical View</span>
                            {/* <a href="/physicalview" style={{ marginLeft: "650px", fontSize: "16px" }}>Click Here to see Physicalview</a> */}
                            <div id={"diagram" + i} style={{ fontSize: "6px", marginTop: "20px" }} ></div>
                          </div>

                        </CardBody>
                      </Card>
                    </Collapse>

                  </div>
                  <div class='card__expander' >

                    <Collapse isOpen={this.state.coll}>
                      <Card>
                        <CardBody>
                          <Button color="primary" onClick={this.toggle} style={{ marginBottom: '1rem', marginLeft: "700px" }}>Click here to see Logical View</Button>
                          <div style={{ width: '1100px', marginTop: "20px", marginLeft:"-50px" }}>

                            <span style={{ color: "red" }}>Physical View</span>
                            {/* <a href="/physicalview" style={{ marginLeft: "650px", fontSize: "16px" }}>Click Here to see Physicalview</a> */}
                            <div id={"diagramTwo" + i} style={{ fontSize: "6px", marginTop: "30px" }} ></div>
                          </div>

                        </CardBody>
                      </Card>
                    </Collapse>

                  </div>

                </div>)

            }
          </div>
          </div>
        </Segment>
      </div>

    );
  }
}
export default Logicalview;


