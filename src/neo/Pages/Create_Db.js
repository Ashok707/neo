import React, { Component } from 'react';
import { Button, Header, Segment, Form,Message, Label,Breadcrumb } from 'semantic-ui-react';
import { createdatasource } from '../../services/reportService';
class createdb extends Component {

    constructor(props) {
        super(props);
        this.state = {

            // id: "a290bf69-4cb3-4be4-94d4-80eb5ac66b2d",
            // name: "OBIEE Development",
            // host: "http://64.79.94.43",
            // port: "9704",
            // username: "weblogic",
            // password: "password@123",
            // dbhost: "64.79.94.43",
            // dbport: "1433",
            // dbusername: "OBI_USER",
            // dbpassword: "password@123"

            name: "",
            host: "",
            port: "",
            username: "",
            password: "",
            dbhost: "",
            dbport: "",
            dbusername: "",
            dbpassword: "",
            showMessage: false,
            status: '200',
            isAvailable: false
        };
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,

        })
        if (this.state.name != null) {
            if (this.state.host != null) {
                if (this.state.port != null) {
                    if (this.state.username != null) {
                        if (this.state.password != null) {
                            if (this.state.dbhost != null) {
                                if (this.state.dbport != null) {
                                    if (this.state.dbusername != null) {
                                        if (this.state.dbpassword != null) {
                            this.setState({
                                isAvailable: true
                            })
                        }
                    }
                }
            }
                        }
                    }
                }
            }
        }
        else {
            this.setState({
                isAvailable: false
            })
        }

    }

    onClick = () => {
//alert("Need API data");
        const user = {
            name: this.state.name,
            host: this.state.host,
            port: this.state.port,
            username: this.state.username,
            password: this.state.password,
            dbhost: this.state.dbhost,
            dbport: this.state.dbport,
            dbusername: this.state.dbusername,
            dbpassword: this.state.dbpassword
        }
        createdatasource(user)
        .then(res => {
            console.log(res);
            var st_code = res.status
            if (st_code === 201) {
                this.setState({
                    showMessage: true,
                    name: '',
                    host: '',
                    port: '',
                    username: '',
                    password: '',
                    dbhost: '',
                    dbport: '',
                    dbusername: '',
                    dbpassword: ''
                })
            }
            else {
                this.setState({
                    showMessage: false
                })
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }

    render() {
        return (
            <div>
              
                <div className="topmenu-body">
                    <Segment>
                        <Header as='h3'>Add OBIEE Information</Header>
                    </Segment>
                    <Breadcrumb>
                        <Breadcrumb.Section href="/home">Activity</Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section href="/filter">OBI Servers</Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section href="/manage" >Manage Servers</Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section active >Create OBIEE Info</Breadcrumb.Section>
                    </Breadcrumb>
                    <Segment>
                        <Header as='h4'>OBIEE</Header>
                        <Form widths='equal'>
                        <Form.Group className={"form-fields"} >
                                <Form.Field className={"fontbold"}>Name</Form.Field>
                                <Form.Field  name="name" value={this.state.name} control='input' onChange={e => this.handleChange(e)} />
                            </Form.Group>
                            <Form.Group className={"form-fields"} >
                                <Form.Field className={"fontbold"}>Host</Form.Field>
                                <Form.Field  name="host" value={this.state.host} control='input' onChange={e => this.handleChange(e)} />
                            </Form.Group>
                            <Form.Group  className={"form-fields"}>
                                <Form.Field className={"fontbold"}>Port</Form.Field>
                                <Form.Field  name="port" value={this.state.port} control='input' onChange={e => this.handleChange(e)} />
                            </Form.Group>
                            <Form.Group  className={"form-fields"}>
                                <Form.Field className={"fontbold"}>User Name</Form.Field>
                                <Form.Field  name="username" value={this.state.username} control='input' onChange={e => this.handleChange(e)} />
                            </Form.Group>
                            <Form.Group  className={"form-fields"}>
                                <Form.Field className={"fontbold"}>Password</Form.Field>
                                <Form.Field  name="password" value={this.state.password} type='password' control='input' onChange={e => this.handleChange(e)} />
                            </Form.Group>
                            <Form.Group  className={"form-fields"}>
                                <Form.Field className={"fontbold"} >RDP Zip</Form.Field>
                                <Form.Field > <Label as="label" basic htmlFor="upload">
                                    <Button icon="upload" label={{ basic: true, content: 'Upload file(s)' }} labelPosition="right" />
                                    <input className={"disp-lock"} hidden id="upload" multiple type="file" /></Label></Form.Field>
                            </Form.Group>
                        </Form>
                    </Segment>

                    <Segment>
                        <Header as='h4'>Database</Header>
                        <Form widths='equal'>
                            <Form.Group  className={"form-fields"}>
                                <Form.Field className={"fontbold"} >Host</Form.Field>
                                <Form.Field className={"form-text-align"}   name="dbhost" value={this.state.dbhost} control='input' onChange={e => this.handleChange(e)} />
                            </Form.Group>
                            <Form.Group  className={"form-fields"}>
                                <Form.Field className={"fontbold"} >Port</Form.Field>
                                <Form.Field   name="dbport" value={this.state.dbport} control='input' onChange={e => this.handleChange(e)} />
                            </Form.Group>
                            <Form.Group  className={"form-fields"}>
                                <Form.Field className={"fontbold"} >User Name</Form.Field>
                                <Form.Field  name="dbusername" value={this.state.dbusername} control='input' onChange={e => this.handleChange(e)} />
                            </Form.Group>
                            <Form.Group  className={"form-fields"}>
                                <Form.Field className={"fontbold"} >Password</Form.Field>
                                <Form.Field  name="dbpassword" value={this.state.dbpassword} type='password' control='input' onChange={e => this.handleChange(e)} />
                            </Form.Group>
                        </Form> 
                        
                    </Segment>
                    <div className={"cdb-div"} >
                        <Button.Group>
                            <Button disabled >Cancel</Button>
                            <Button.Or />
                            <Button positive onClick={this.onClick.bind()}>Save</Button>
                        </Button.Group>
                    </div>
                    {this.state.showMessage && (
                            <div style={{ marginTop: '-270px', width: '300px', marginLeft: '250px' }} >
                                <Message positive>
                                    <Message.Header>Registered Successfully</Message.Header>
                                    <p>
                                        <a href="/home"><b>Click here</b></a> to manage your server
                                        </p>
                                </Message>
                            </div>
                        )}  
                </div>
            </div>
        );
    }

}


export default createdb;