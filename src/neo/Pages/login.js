import React from 'react'
import { Button, Form, Grid, Header, Image, Segment } from 'semantic-ui-react'
//import LoginLayout from '../layout/LoginLayout'
const LoginForm = () => (
  <div>
    {/* <LoginLayout></LoginLayout> */}
    <Grid textAlign="center" verticalAlign='middle'>
      <Grid.Column className={"log-frm-size"}>
        <Header as='h3' textAlign='center'>
          <Image src='./neo-logo.png' /> Log-in to your account
        </Header>
        <Form size='large'>
          <Segment >
            <Form.Input fluid icon='user' iconPosition='left' placeholder='E-mail address' />
            <Form.Input fluid icon='lock' iconPosition='left' placeholder='Password' type='password' />
            <Button color='teal' href='/home' fluid size='large'>
              Login
            </Button>
            <div className={"log-foo"}>
              <div className={"log-foo-l"}><a href="/">Forgot Password?</a></div>
              <div className={"log-foo-r"}>New to us? <a href="/">Sign Up</a></div>
            </div>
          </Segment>
        </Form>

      </Grid.Column>
    </Grid>
  </div>
)

export default LoginForm