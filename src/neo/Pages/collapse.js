import React from 'react';
import { Segment, Input } from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css';
import $ from 'jquery';
import kendo from '@progress/kendo-ui';
import { getlogicalData, getlogicalDatadigram } from '../../services/reportService';

class collapse extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
        //let onDataBound = [];
        //let hideChildren = [];
        
        }  

        

        componentDidMount(){
            var visualTemplate = (options) => {
                var dataviz = kendo.dataviz;
                var g = new dataviz.diagram.Group();
                var dataItem = options.dataItem;
                var currY = 30
                g.append(new dataviz.diagram.Rectangle({
                  width: 150,
                  height: 30,
                  stroke: {
                    width: 1,
                    color: "#3399dd"
                  },
                  fill: {
                    color: "#3399dd"
                  }
                }));
          
          
                g.append(new dataviz.diagram.TextBlock({
                  text: dataItem.name.toUpperCase(),
                  // text: <i class="fa fa-bars"></i>,
                  x: 40,
                  y: 10,
                  fill: "#fff",
                  fontSize: 10,
          
                }));
          
          
          
                // for (var i = 0; i < dataItem.columns.length; i++) {
                //   currY += 15;
                //   g.append(new dataviz.diagram.TextBlock({
                //     text: dataItem.columns[i].name,
                //     x: 5,
                //     y: currY,
                //     fill: "#000",
                //     fontSize: 10
                //   }));
                //   g.append(new dataviz.diagram.TextBlock({
                //     text: dataItem.columns[i].type,
                //     x: 100,
                //     y: currY,
                //     fill: "#000",
                //     fontSize: 10
                //   }));
                // }
                // g.append(new dataviz.diagram.Rectangle({
                //   width: 150,
                //   height: dataItem.columns.length + currY + 15,
                //   stroke: {
                //     width: 1,
                //     color: "#3399dd"
                //   },
                //   fill: {
                //   }
                // }));
          
                $("#loading").find("div.k-loading-mask").remove();
                return g;
              }
            var hideChildren=(shape, diagram, shouldShow)=>{

                var connections = shape.connections("out");
                console.log(connections);
                if (connections.length !== 0) {
                  for(var i = 0; i < connections.length; i++){
                    var shape = connections[i].target().shape;
                    var showShape = typeof shouldShow !== "undefined" ? shouldShow : !shape.visible();
                    if(shape.connections("out").length > 0){
                      hideChildren(shape, diagram, showShape);
                    }
                    var inConnections = shape.connections("in");
        
                    inConnections[0].visible(showShape);
                    shape.visible(showShape);
                  }
                }  
              }  
              var handleSelect = (e) => {
                   console.log(e.sender,"diagram");
                   console.log(e.item,"diagram-item");
                var diagram = e.sender;
                      
                var item = e.item;
                if(e.item instanceof kendo.dataviz.diagram.Shape){
                    var dataviz = kendo.dataviz;
                    var g = new dataviz.diagram.Group();
                    console.log(e.item.dataItem,"diagram-e.dataItem");
                    var dataItem = e.item.dataItem;
                    console.log(dataItem,"diagram-dataItem");
                var currY = 30
                    //console.log(g);
                  //hideChildren(item, diagram);
                  for (var i = 0; i < dataItem.columns.length; i++) {
                      currY += 15;
                      g.append(new dataviz.diagram.TextBlock({
                        text: dataItem.columns[i].name,
                        x: 5,
                        y: currY,
                        fill: "#000",
                        fontSize: 10
                      }));
                      g.append(new dataviz.diagram.TextBlock({
                        text: dataItem.columns[i].type,
                        x: 100,
                        y: currY,
                        fill: "#000",
                        fontSize: 10
                      }));
                    }
                    g.append(new dataviz.diagram.Rectangle({
                      width: 150,
                      height: dataItem.columns.length + currY + 15,
                      stroke: {
                        width: 1,
                        color: "#3399dd"
                      },
                      fill: {
                      }
                    }));
                }
              }
            var  onDataBound=(e)=> {
                var that = this;
                setTimeout(function () {
                //   that.bringIntoView(that.shapes);
                }, 0);
              } 
           
              const user = {
                "path": "/shared/ Northwind & Folder Name/$Northwind Orders Report-based on subquery",
              "server_id": "271b4030-e494-49f3-b478-54169ec877b1"
             }

              getlogicalDatadigram(user)
              .then((response) => {
                let diagramdata = response.data;
                let diagramconnection = [];
                var input = diagramdata;
                var x = 50, y = 50, z = 0;
                var currxy = 0;
                for (var i = 0; i < input.length; i++) {
                    if (input[i].to) {
                      var connectto = input[i].to.length;
                      for (var j = 0; j < connectto; j++) {
                        var row = {}
                        row["from"] = input[i].id;
                        row["to"] = input[i].to[j];
                        row["text"] = input[i].name;
                        diagramconnection.push(row);
                      }
                    }
                    if (z === 0) {
                      diagramdata[i]["x"] = x;
                      diagramdata[i]["y"] = y;
                    }
                    else {
                      if (i % 2 === 0) {
                        //currxy = -250;
                      }
                      else {
                        currxy = 50;
                      }
                      x += 120;
                      y += currxy;
                      diagramdata[i]["x"] = x;
                      diagramdata[i]["y"] = y;
                    }
                    z++;
                  }
                  $("#diagram").kendoDiagram({
                    dataSource: {
                        data: diagramdata,
                        schema: {
                          model: {
                            id: "id"
                          }
                        }
                      },
                      connectionsDataSource: {
                        data: diagramconnection
                      },
                      layout: null,
                      shapeDefaults: {
                        visual: visualTemplate
                      },
                      editable: {
                        tools: false
                      },
                      connectionDefaults: {
                        endCap: {
                          type: "ArrowEnd",
                          fill: "#228B22"
                        },
                        stroke: {
                          color: "#6A5ACD",
                          width: 1
                        },
                        content: {
                          template: "#:dataItem.text#",
                          fontSize: 10,
                          color: "#FF0000"
                        }
                      },
                    dataBound: onDataBound,
                    click: handleSelect
                  });
              });

        
       
    }
   

 
    
    render() {
        return ( <div>
            <Segment>
              <h3>Logical View</h3>
    
            </Segment>
            <Segment>
            <div id="example">
      <div id="diagram" style={{height:"600px"}}></div>
      </div>
            </Segment>
            </div>)
    }
}
export default collapse;