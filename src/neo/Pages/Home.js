import React, { Component } from "react";
import { Button } from 'semantic-ui-react'
import { Segment ,Header} from "semantic-ui-react";

export default class Home extends Component {
    lastSelectedIndex = 0;
    CategoryFilterCell;
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <div>
                <div className="topmenu-body">
                    <Segment>
                        <Header as ='h3'>Manage Your Activity</Header>
                    </Segment>
                    <Segment className={"hm-seg"}>                   
                        <a href="/filter"  className={"hm-seg-m-l"}>
                            <Button color='teal'  className={"hm-seg-m-l-btn"}  >Convert OBI Reports </Button></a>
                        <br /><br />
                        <a href="/neoreports" className={"hm-seg-m-l"} >
                            <Button color='teal' className={"hm-seg-m-l-btn"}>Create/Update/View Neo Reports</Button></a></Segment>
                </div>
                </div>
           
        );
    }
}

