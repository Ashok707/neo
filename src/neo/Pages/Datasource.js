import React, { Component } from 'react';
import axios from 'axios';
import { Button, Form, Message, Segment,Label } from 'semantic-ui-react';
import Layout from '.././layout/Layout';
import {allservers} from '../../services/reportService'

class Register extends Component {
    constructor() {
        super()
        this.state = {
            name: '',
            host: '',
            port: '',
            username: '',
            password: '',
            showMessage: false,
            status: '200',
            isAvailable: false
        }

    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,

        })
        if (this.state.name != null) {
            if (this.state.host != null) {
                if (this.state.port != null) {
                    if (this.state.username != null) {
                        if (this.state.password != null) {
                            this.setState({
                                isAvailable: true
                            })
                        }
                    }
                }
            }
        }
        else {
            this.setState({
                isAvailable: false
            })
        }

    }

    onClick = () => {

        const user = {
            host: this.state.host,
            port: this.state.port,
            username: this.state.username,
            password: this.state.password,
            name: this.state.name
        }

        
        var headers = {
            'Content-Type': 'application/json'  
        }
        axios.post(`${allservers.servers}`, user,{headers: headers})
            .then(res => {
                // console.log('ok')
                // console.log(res.status)
                var st_code = res.data.status
                if (st_code === 201) {
                    this.setState({
                        showMessage: true, 
                        name: '',
                        host: '',
                        port: '',
                        username: '',
                        password: ''
                    })
                }
                else {
                    this.setState({
                        showMessage: false
                    })
                }

            })

            .catch((error) => {
                console.error(error);
            });
    }
    render() {

        return (

            <div className='login-form' style={{ marginTop: "-100px" }} >

                <Layout>
                    <div style={{ marginTop: "100px" }}>
                        <Segment>
                            <h3 >Register Your Datasource </h3>
                        </Segment>
                    </div>
                    <Segment >
                        <Form onSubmit={this.onSubmit} center style={{ width: '50%' }} >
                        <Form.Field inline >
                        <Label pointing='right' color="blue">Server Name </Label>
                        <input style={{ marginLeft: '2px' }}  name="name" 
                        value={this.state.name} onChange={e => this.handleChange(e)}/>
                    </Form.Field>
                    <Form.Field inline>
                        <Label pointing='right' color="blue">Host </Label>
                        <input style={{ marginLeft: '45px' }}  name="host" 
                        value={this.state.host} onChange={e => this.handleChange(e)} />
                    </Form.Field>
                    <Form.Field inline>
                        <Label pointing='right' color="blue">Port</Label>
                        <input style={{ marginLeft: '48px' }}  name="port"
                        value={this.state.port} onChange={e => this.handleChange(e)}/>
                    </Form.Field>
                    <Form.Field inline>
                        <Label pointing='right' color="blue"> Username </Label>
                        <input style={{ marginLeft: '16px' }}  name="username"
                        value={this.state.username} onChange={e => this.handleChange(e)}/>
                    </Form.Field>
                    <Form.Field inline>
                        <Label pointing='right' color="blue">Password</Label>
                        <input style={{ marginLeft: '19px' }} type='password' name="password"
                        value={this.state.password} onChange={e => this.handleChange(e)}/><br/>
                    </Form.Field>

                            <div style={{ marginLeft: '10em' }}>
                                <Button type="submit" color='teal' onClick={this.onClick.bind()}
                                    disabled={!this.state.name || !this.state.host || !this.state.port || !this.state.username || !this.state.password}>Connect</Button>
                            </div>
                        </Form>
                        {this.state.showMessage && (
                            <div style={{ marginTop: '-270px', width: '300px', marginLeft: '250px' }} >
                                <Message positive>
                                    <Message.Header>Registered Successfully</Message.Header>
                                    <p>
                                        <a href="/home"><b>Click here</b></a> to manage your server
                                        </p>
                                </Message>
                            </div>
                        )}
                    </Segment>

                </Layout>
            </div>
        );
    }
}

export default Register;
