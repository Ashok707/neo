import { myConfig } from '../config/config';
import axios from 'axios';

var headers =
{
  'Content-Type': 'application/json'
}

export const singleservers =
{
  servers: `${myConfig.ServerapiUrl}/servers/${localStorage.ServerID}`
}

export const nodesingleservers =
{
  servers: `${myConfig.ServerapiUrl}/servers/connect/${localStorage.ServerID}`
}

export const allservers =
{
  servers: `${myConfig.ServerapiUrl}/servers/`
}

export const connectservers =
{

  servers: `${myConfig.ServerapiUrl}/servers/connect/`

}

export const subitems =
{
  reports: `${allservers.servers}${localStorage.ServerID}/subitems`

}

export const neoreports =
{
  path: `${myConfig.ServerapiUrl}/neo/reports`
}

export const runreport =
{
  runreportserverid: `${myConfig.ServerapiUrl}/servers/${localStorage.ServerID}/runreport`
}

export const getnodeData = () => {
  var id= localStorage.getItem('ServerID')
  return axios
      .get(`${myConfig.ServerapiUrl}/servers/connect/${id}`, headers )
      .then(res => res  )
}

export const getneoreportdata = Userdata => {
  var id= localStorage.getItem('ServerID')
  return axios
      .post(`${myConfig.ServerapiUrl}/servers/${id}/runreport`, Userdata,{headers: headers})
      .then(res => res)
}

export const reportdetails =Userdata=>{
  var id= localStorage.getItem('ServerID')
  return axios
  .post( `${myConfig.ServerapiUrl}/servers/${id}/reportdetails`, Userdata,{headers: headers})
  .then(res => res)
}

export const LoadsubItems = Userdata => {
  var user = JSON.stringify(Userdata)
  var id= localStorage.getItem('ServerID')
  return axios
      .post(`${myConfig.ServerapiUrl}/servers/${id}/subitems`, user,{headers: headers})
      .then(res => res)
}

export const convertReports=Userdata =>{
  
  return axios 
  .post( `${myConfig.ServerapiUrl}/neo/convertReports`, Userdata,{headers: headers})
  .then(res => res)
}

export const createdatasource=Userdata =>{
  
  return axios 
  .post( `${allservers.servers}`, Userdata,{headers: headers})
  .then(res => res)
}


export const getlogicalData = () => {
  return axios
      .get(`${neoreports.path}`, headers )
      .then(res => res  )
}

// export const getlogicalDatadigram=Userdata =>{
  
//   return axios 
//   .post( `${myConfig.ServerapiUrl}/neo/logical`, Userdata,{headers: headers})
//   .then(res => res)
// }

export const getPhysicalDatadigram=Userdata =>{

  return axios 
  .post( `${myConfig.ServerapiUrl}/neo/physical`, Userdata,{headers: headers})
  .then(res => res)
}

export const getlogicalDatadigram = (Userdata, getphysicalDatadigram) => {
  let url = '';
  if (getphysicalDatadigram === "logicaldiagram")
    url = `${myConfig.ServerapiUrl}/neo/logical`;
  else if (getphysicalDatadigram === "physicaldiagram")
    url = `${myConfig.ServerapiUrl}/neo/physical`;
  return axios
    .post(url, Userdata, { headers: headers })
    .then(res => res)
}